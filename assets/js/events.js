var event_id = '';
var current_element = '';
var event_description = '';
var event_name = '';
var event_page = '';
var cancel_alert_type = '';
var cancel_alert_message = '';

function deferredLoad(target, url) {
    return $.Deferred(function(deferred) {
        $(target).load(url, function() {
            deferred.resolve();
        });
    }).promise();
}

$(document).ready(function(){
    check_guest();
    $('.loadingDiv').hide();
    close_alert();
});

$(document).on("hidden.bs.modal", ".guest-modal", function () {
    check_guest();
});

$(function(){
    check_guest();
});

function check_guest(){
    $('.loadingDiv').show();
    $('.add_guests_button, .edit_guests_button').each(function(){
        var get_this = this;
        var check_event_id = $(this).data('event_id');
        console.log(check_event_id);
        $.ajax({
            type: "GET", 
            url: window.base_url+'member/events/check_has_guest', 
            data: {event_id: check_event_id},
            dataType : 'JSON',
            //context: this,
            success: function (response) {
                if(response.count_guests > 0){
                    if($(get_this).hasClass('add_guests_button')){
                        $(get_this).addClass('edit_guests_button');
                        $(get_this).removeClass('add_guests_button');
                        $(get_this).text('Edit Guests');
                    }
                } else if(response.count_guests == 0){
                    if($(get_this).hasClass('edit_guests_button')){
                        $(get_this).addClass('add_guests_button');
                        $(get_this).removeClass('edit_guests_button');
                        $(get_this).text('Add Guests');
                    }
                }
                $('.loadingDiv').hide();
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    });
}

function close_alert(){
    $(".alert").fadeTo(5000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });
}

$(document).on('click', 'button.add_guests_button', function(){
    $('.loadingDiv').show();
    event_id = $(this).data('event_id');
    var guest_bootbox = bootbox.alert({
        title: 'filler',
        className: 'guest-modal',
        message: 'filler',
        closeButton: true
    });

    guest_bootbox.init(function(){
        //$('.guest-modal').find('.modal-body').empty();
        var bootbox_content = $(this);
        bootbox_content.find('.modal-body').addClass('bg-pink');
        bootbox_content.find('.modal-body').load(window.base_url + 'member/events/guest_form?event_id='+ event_id);
        bootbox_content.find('.modal-footer').hide();
        $('.guest-modal').find('.modal-header').text('');
        $('.guest-modal').find('.modal-header').html('<button class="bootbox-close-button close" type="button">×</button>');
        $('.guest-modal').find('.modal-header').css('background-color','#FBF1ED');
        $('.guest-modal').find('.modal-header').css('border-bottom','none');
        $('.guest-modal').find('.modal-header').css('padding','15px 15px 0 0');
        $('.loadingDiv').hide();
    });
});

$(document).on('click', 'button.cancel_add_guest', function(){
    $('.guest-modal').modal('hide');
    $('#add-guest-form')[0].reset();
    window.location = window.base_url + 'events';
});

$(document).on('click', 'button.cancel_edit_guest', function(){
    $('.guest-modal').modal('hide');
    $('#edit-guest-form')[0].reset();
    //window.location = window.base_url + 'events';
});

$(document).on('click', 'button.cancel_delete_guest', function(){
    $('.guest-modal').modal('hide');
    $('#delete-guest-form')[0].reset();
    //window.location = window.base_url + 'events';
});

$(document).on('click', 'button.clear_add_guest', function(){
    $('#add-guest-form')[0].reset();
});

$(document).on('click', 'button.save_only_guest', function(){
    $(this).attr('disabled', 'disabled');
    $('.loadingDiv').show();
    if($('input#guest_email').val() == null){
        $('label.error').css('display', 'none');
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da;')
    }
    if($("#add-guest-form").valid()){
        var add_guest_data = $("#add-guest-form").serializeArray();
        $.ajax({
            type: "POST", 
            url: window.base_url+'events/add_guest', 
            data: add_guest_data,
            dataType : 'JSON',
            success: function (response) {
                console.log(response);
                $('.loadingDiv').hide();
                $('#add-guest-form')[0].reset();
                var alert_type = 'warning';
                if(response.success == true){
                    alert_type = 'success';
                }
                $('.guest-modal').find('div.add_guest_alert').html('<div class="alert alert-' + alert_type + '" role="alert">' + response.message + '</div>');
                if(response.warning_message != ''){
                    $('.guest-modal').find('div.add_guest_alert').append('<div class="alert alert-warning" role="alert">' + response.warning_message + '</div>');
                }
                if(response.remaining_guest_pass == 0 && response.remaining_guest_pass != 'unlimited'){
                    $('.guest-modal').find('div#with_guest_pass').css('display', 'none');
                    $('.guest-modal').find('input#guest_first_name').attr('disabled', 'disabled');
                    $('.guest-modal').find('input#guest_last_name').attr('disabled', 'disabled');
                    $('.guest-modal').find('input#guest_email').attr('disabled', 'disabled');
                    $('.guest-modal').find('button.save_add_guest').attr('disabled', 'disabled');
                    $('.guest-modal').find('button.clear_add_guest').attr('disabled', 'disabled');
                } else {
                    $('.guest-modal').find('div#with_guest_pass').css('display', 'block');
                    $('.guest-modal').find('span.available_guest_count').text(response.remaining_guest_pass);
                    $('button.save_add_guest').removeAttr('disabled');
                }
                $('form#add-guest-form > div.form-group').hide();
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    } else {
        $('.loadingDiv').hide();
        $('label.error').css('display', 'none');
        $(this).removeAttr('disabled');
    }
});

$(document).on('click', 'button.save_add_guest', function(){
    $('.loadingDiv').show();
    $('form#add-guest-form > div.form-group').show();
    $('button.save_only_guest').attr('disabled', 'disabled');
    $(this).attr('disabled', 'disabled');
    $('button.save_only_guest').removeAttr('disabled');
    $('.loadingDiv').hide();
});

$(document).on('click', 'button.rsvp_button', function(){
    $('.loadingDiv').show();
    event_id = $(this).data('event_id');
    current_element = $(this);
    $.ajax({
        type: "POST", 
        url: window.base_url+'events/rsvp', 
        data: {event_id : event_id},
        dataType : 'JSON',
        success: function (response) {
            if(response.success == true){
                $('.loadingDiv').hide();
                var guest_bootbox = bootbox.alert({
                    className: 'guest-modal',
                    message: 'filler'
                });
            
                guest_bootbox.init(function(){
                    $(this).find('.modal-body').addClass('bg-pink');
                    $('.modal-body').empty();
                    $('.modal-body').load(window.base_url + 'member/events/guest_form?event_id='+ event_id + '&rsvp=success');
                    $(this).find('.modal-footer').hide();
                    $('.loadingDiv').hide();
                    close_alert();
                });
                /*
                $('div.events_alert').html('');
                event_description = current_element.closest('div.cal-desc').find('div.event_description').html();
                event_name = current_element.closest('div.cal-desc').find('input.event_name').val();
               var loadPromises = [];
               loadPromises.push(deferredLoad('div.events-body', window.base_url + 'events/details'));
               $.when.apply(null, loadPromises).done(function() {
                    $('div.events-body').find('h5.event-name').text(event_name);
                    $('div.events-body').find('p.event-description').html(event_description);
                    $('div.events-body').find('h5.attendees-list').text('Attendees (' + response.count + ')');
                    $('.loadingDiv').show();
                    loadPagination(0, event_id);
               });
               */
            }
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
            console.log("There was an error: " + errorThrown);  
        }
    });
});
$(document).on('click','#pagination a',function(e){
    e.preventDefault(); 
    var pageno = $(this).attr('data-ci-pagination-page');
    $('.loadingDiv').show();
    loadPagination(pageno, event_id);
});

var get_event_id = '';
// Load pagination
function loadPagination(pagno, get_event_id){
    $.ajax({
        url: window.base_url + 'member/events/loadRecord/'+pagno,
        type: 'get',
        data: {'event_id': get_event_id},
        dataType: 'json',
        success: function(response){
            $('.loadingDiv').hide();
            $('#pagination').html(response.pagination);
            createTable(response.result,response.row);
        }
    });
}


function createTable(result,sno){
    sno = Number(sno);
    $('div#attendee-list-holder').empty();
    var list = '';
    if(result != ''){
        list = '<ul class="list-group">';
        for(index in result){
            sno+=1;
            var at = '';
            if(result[index].position != null && result[index].company != null){
                at = ' at ';
            } else {
                if((result[index].position == null && result[index].company != null) || (result[index].position != null && result[index].company == null)){
                    at = '';
                }
            }
            var profile_image = result[index].profile_image;
            if(profile_image == null){
                profile_image = 'assets/images/default.png';
            }
            var industry = result[index].industry;
            list += '<li class="list-group-item bg-pink">';
            list += '<div class="row p-4">';
            list += '<div class="col-md-3">';
            list += '<img style="width: 100px;height: 100px;" src="' + window.base_url + profile_image + '" />';
            list += '</div>';
            list += '<div class="col-md-9">';
            list += '<table class="attendee-list">';
            list += '<tbody>';
            list += '<tr>';
            list += '<td><h5>' + result[index].first_name + ' ' + result[index].last_name + '</h5></td>';
            list += '</tr>';
            if(result[index].position != null || result[index].company != null){
                list += '<tr>';
                list += '<td><h6>' + result[index].position + at + result[index].company + '</h6></td>';
                list += '</tr>';
            }
            if(industry != null){
                list += '<tr>';
                list += '<td><p><strong>' + industry + '</strong></p></td>';
                list += '</tr>';
            }
            /*
            if(result[index].braintree_plan_id != null){
                list += '<tr>';
                list += '<td><p><strong>' + result[index].braintree_plan_id + '</strong></p></td>';
                list += '</tr>';
            }
            */
            list += '</tbody>';
            list += '</table>';
            list += '</div>';
            list += '</div>';
            list += '</li>';
        }
        list += '</ul>';
    }
    //$('div.attendeeslist_holder').append(list);
    //$('div#add-guest-form-content').append(list);
    $('.attendee-modal').find('#attendee-list-holder').html(list);
    $('.attendee-modal').find('.loadingDiv').hide();
}

$(document).on('click', 'button.rsvp_cancel_button', function(){
    event_id = $(this).data('event_id');
    current_element = $(this);
    event_name = current_element.closest('div.cal-desc').find('input.event_name').val();
    bootbox.confirm({
        title: "Confirm Cancellation",
        message: '<p class="text-left">Are you sure you want to cancel going to <strong>' + event_name + '</strong>?</p>',
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-brunchwork'
            },
            cancel: {
                label: 'No',
                className: 'btn-brunchwork'
            }
        },
        callback: function (result) {
            console.log(result);
            if(result == null || result == false) {
                $(this).modal('hide');
            } else {
                if(result == true){
                    $.ajax({
                        url: window.base_url + 'events/cancel_rsvp',
                        type: "POST",
                        dataType:'JSON',
                        data: {event_id : event_id},
                        success: function(response) {
                            $(window).scrollTop(0);
                            if(response.success == true){
                                cancel_alert_type = 'success';
                                cancel_alert_message = 'You successfully cancelled going to event: <strong>' + event_name + '</strong>.';
                            } else {
                                cancel_alert_type = 'danger';
                                cancel_alert_message = 'Something went wrong. You can try refreshing the page and try again. If issue persists, please contact support. ';
                            }
                            current_element.addClass('rsvp_button');
                            current_element.removeClass('rsvp_cancel_button');
                            current_element.text('RSVP');
                            $('div.events_alert').html('<div class="alert alert-' + cancel_alert_type + '" role="alert">' + cancel_alert_message + '</div>');
                            $(".alert").fadeTo(5000, 500).slideUp(500, function(){
                                $(".alert").slideUp(500);
                                $('div.events_alert').html('');
                            });
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                          console.log("There was an error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
});
/*
$(document).on('change', '#member_city', function(){
    var city = $(this).find('option:selected').data('search');
    //location.reload(window.base_url + '?city=' + city);
    window.location.href = window.location.href.replace( /[\?#].*|$/, '?city=' + city );
});
*/
$(document).on('click', 'button.search-membercity', function(){
    var city = $('#member_city').find('option:selected').data('search');
    window.location.href = window.location.href.replace( /[\?#].*|$/, '?city=' + city );
});

$(document).on('click', 'button.details_button', function(){
    var region = $(this).data('region');
    $('.loadingDiv').show();
    //$('div#content').load(window.base_url + 'events/details?id='+ $(this).data('event_id'));
    var loadPromises = [];
    loadPromises.push(deferredLoad('div#content', window.base_url + 'events/details?id='+ $(this).data('event_id')));
    $.when.apply(null, loadPromises).done(function() {
        $('div#content').find('h2.region').text(region);
    });
});

$(document).on('click', 'button.attendee-list', function(){
    $('div#content > .loadingDiv').show();
    event_id = $(this).data('event_id');
    /*
    var csrf = $("#csrf").attr("name");
    var csrf_value = $("#csrf").val();
    $('div#add-guest-form-content').html('');
    */
    $.ajax({
        type: "GET", 
        url: window.base_url+'events/get_attendees', 
        data: {event_id : event_id},
        dataType : 'JSON',
        success: function (response) {
            var initial_message = '';
            var response_counter = response.count;
            if(response.count == 0){
                initial_message = '<p>Note: Invited guests are not included in this list.</p><div id="attendee-list-holder">There are no attendees in this event.</div><div class="center-parent loadingDiv"><div class="center-container"></div></div>';
            } else {
                initial_message = '<p>Note: Invited guests are not included in this list.</p><div id="attendee-list-holder"></div><div class="center-parent loadingDiv"><div class="center-container"></div></div><div style="margin-top: 10px;" id="pagination"></div>';
                loadPagination(0, event_id);
            }
            var attendee_bootbox = bootbox.alert({
                title: 'Attendee List (' + response.count + ')',
                className: 'attendee-modal',
                message: initial_message,
                closeButton: true
            }).on('shown.bs.modal', function (e) {
                if(response_counter == 0){
                    $('.attendee-modal').find('.loadingDiv').hide();
                } else {
                    $('.attendee-modal').find('.loadingDiv').show();
                }
            });
            
            attendee_bootbox.init(function(){
                $('div#content > .loadingDiv').hide();
                var bootbox_content = $(this);
                bootbox_content.find('.modal-body').addClass('bg-pink');
                bootbox_content.find('.modal-footer').hide();
                bootbox_content.find('.modal-dialog').addClass('modal-lg');
                bootbox_content.find('.modal-footer').addClass('bg-pink');
                bootbox_content.find('.modal-footer').addClass('text-right');
                bootbox_content.find('.modal-footer').css('display', 'block');
                bootbox_content.find('.modal-footer').hide();
            });
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
            console.log("There was an error: " + errorThrown);  
        }
    });
});

$(document).on('close', 'button.close-modal', function(){
    $('.guest-modal').modal('hide');
    $('.guest-modal').find('.modal-dialog').removeClass('modal-lg');
    $('.guest-modal').find('.modal-footer').css('display', 'none');
});
/*
$(document).on('hidden.bs.modal', '.guest-modal', function () {
    $('.loadingDiv').show();
    location.reload();
})
*/
$(document).on('click', '.edit_guests_button', function(){
    $('.loadingDiv').show();
    event_id = $(this).data('event_id');
    //check if has guests
    $.ajax({
        type: "GET", 
        url: window.base_url+'member/events/check_has_guest', 
        data: {event_id:event_id},
        dataType : 'JSON',
        success: function (response) {
            $('.loadingDiv').hide();
            if(response.guests == 0){
                var guest_bootbox = bootbox.alert({
                    title: "Guest Options",
                    className: 'guest-modal',
                    message: '<div class="row"><div class="col-md text-center"><button class="btn btn-brunchwork add_guests">Add Guests</button></div>'
                });
            } else {
                var guest_bootbox = bootbox.alert({
                    title: "Guest Options",
                    className: 'guest-modal',
                    message: '<div class="row"><div class="col-md"><button class="btn btn-brunchwork add_guests">Add Guests</button></div><div class="col-md"><button class="btn btn-brunchwork edit_guests">Edit Guests</button></div><div class="col-md"><button class="btn btn-brunchwork delete_guests">Delete Guests</button></div></div>'
                });
            }
            guest_bootbox.init(function(){
                $(this).find('.modal-body').addClass('bg-pink');
                //$('.modal-body').empty();
                //$('.modal-body').load(window.base_url + 'member/events/guest_form?event_id='+ event_id);
                $(this).find('.modal-footer').hide();
                $('.loadingDiv').hide();
            });
        },
            error: function (MLHttpRequest, textStatus, errorThrown) {
            console.log("There was an error: " + errorThrown);  
        }
    });
});

$(document).on('click', '.add_guests', function(){
    $('.loadingDiv').show();
    $('.guest-modal').find('.modal-body').empty();
    //$('.guest-modal').find('.modal-header').css('display', 'none');
    $('.guest-modal').find('.modal-header').html('<button class="bootbox-close-button close" type="button">×</button>');
    $('.guest-modal').find('.modal-header').css('background-color','#FBF1ED');
    $('.guest-modal').find('.modal-header').css('border-bottom','none');
    $('.guest-modal').find('.modal-header').css('padding','15px 15px 0 0');
    $('.modal-body').load(window.base_url + 'member/events/guest_form?event_id='+ event_id);
    $('.loadingDiv').hide();
});

$(document).on('click', '.edit_guests', function(){
    $('.loadingDiv').show();
    $('.guest-modal').find('.modal-body').empty();
    //$('.guest-modal').find('.modal-header').css('display', 'none');
    $('.guest-modal').find('.modal-header').html('<button class="bootbox-close-button close" type="button">×</button>');
    $('.guest-modal').find('.modal-header').css('background-color','#FBF1ED');
    $('.guest-modal').find('.modal-header').css('border-bottom','none');
    $('.guest-modal').find('.modal-header').css('padding','15px 15px 0 0');
    $('.modal-body').load(window.base_url + 'member/events/edit_guest_form?event_id='+ event_id);
});

$(document).on('click', '.delete_guests', function(){
    $('.loadingDiv').show();
    $('.guest-modal').find('.modal-body').empty();
    //$('.guest-modal').find('.modal-header').css('display', 'none');
    $('.guest-modal').find('.modal-header').html('<button class="bootbox-close-button close" type="button">×</button>');
    $('.guest-modal').find('.modal-header').css('background-color','#FBF1ED');
    $('.guest-modal').find('.modal-header').css('border-bottom','none');
    $('.guest-modal').find('.modal-header').css('padding','15px 15px 0 0');
    $('.modal-body').load(window.base_url + 'member/events/delete_guest_form?event_id='+ event_id);
});

$(document).on('click', '.update_edit_guest', function(){
    if($("#edit-guest-form").valid()){
        $('.loadingDiv').show();
        $.ajax({
            type: "POST", 
            url: window.base_url+'member/events/update_guest', 
            data: $('#edit-guest-form').serializeArray(),
            dataType : 'JSON',
            success: function (response) {
                $('.loadingDiv').hide();
                var message = '';
                var new_guest_list = response.guests;
                if(response.count > 0){
                    $('#edit_guest_dropdown').html('');
                    var append_data = '';
                    append_data += '<option value="" selected disabled>Select guest to edit.</option>';
                    $(new_guest_list).each(function(i,e){
                        console.log(e);
                        append_data += '<option value="'+e.id+'" data-first_name="'+e.first_name+'" data-last_name="'+e.last_name+'" data-email="'+e.email+'">'+e.first_name+' '+e.last_name+'</option>';
                    });
                    $('#edit_guest_dropdown').append(append_data);
                }
                $('.guest-modal').find('#edit_guest_dropdown').val($('.guest-modal').find('input#guest_id').val());
                if(response.success == true){
                    message = 'The guest you selected was successfully updated.';
                } else {
                    message = 'Something went wrong. Please try again.';
                }
                $('.guest-modal').find('.edit_guest_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert">' + message + '</div>');
                close_alert();
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        }); 
    }
});

$(document).on('change', '#edit_guest_dropdown', function(){
    var edit_guest = $(this);
    $('.hidden').removeAttr('style');
    $('.guest-modal').find('input#guest_id').val(edit_guest.val());
    $('.guest-modal').find('input#guest_first_name').val(edit_guest.find(':selected').data('first_name'));
    $('.guest-modal').find('input#guest_last_name').val(edit_guest.find(':selected').data('last_name'));
    $('.guest-modal').find('input#guest_email').val(edit_guest.find(':selected').data('email'));
});

$(document).on('change', '#delete_guest_dropdown', function(){
    var delete_guest = $(this).val();
    $('.hidden').removeAttr('style');
    $('.guest-modal').find('input#guest_id').val(delete_guest);
});

$(document).on('click', '.update_delete_guest', function(){
    var delete_guest = $('#delete_guest_dropdown option:selected').data('first_name') + ' ' + $('#delete_guest_dropdown option:selected').data('last_name');
    $(this).text('Continue');
    $(this).removeClass('update_delete_guest');
    $(this).attr('id', 'continue_delete_guest');
    $('.guest-modal').find('.hide-if-continue').css('display', 'none');
    $('.guest-modal').find('.delete_guest_alert').html('<div class="alert alert-warning" role="alert">Are you sure you want to delete <strong>' + delete_guest + '</strong> as your guest in this event?</div>');
    $('.guest-modal').find('#back_delete_span').css('visibility', 'visible');
});

$(document).on('click', '#continue_delete_guest', function(){
    if($("#delete-guest-form").valid()){
        $('.loadingDiv').show();
        $.ajax({
            type: "POST", 
            url: window.base_url+'member/events/delete_guest', 
            data: $('#delete-guest-form').serializeArray(),
            dataType : 'JSON',
            success: function (response) {
                if(response.count > 0){
                    $('#delete_guest_dropdown').html('');
                    var append_data = '';
                    var new_guest_list_delete = response.guests;
                    append_data += '<option value="" selected disabled>Select guest to delete.</option>';
                    $(new_guest_list_delete).each(function(i,e){
                        append_data += '<option value="'+e.id+'" data-first_name="'+e.first_name+'" data-last_name="'+e.last_name+'" data-email="'+e.email+'">'+e.first_name+' '+e.last_name+'</option>';
                    });
                    $('#delete_guest_dropdown').append(append_data);
                } else if(response.count == 0){
                    $('form#delete-guest-form').hide();
                }
                /*
                else if(response.count == 0){
                    $('.guest-modal').modal('hide');
                    location.reload();
                }
                */
                $('.guest-modal').find('.delete_guest_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert">' + response.alert_message + '</div>');
                $('.guest-modal').find('#delete_guest_dropdown').val('');
                $('.guest-modal').find('#back_delete_span').css('visibility', 'hidden');
                $('.guest-modal').find('#continue_delete_guest').addClass('update_delete_guest');
                $('.guest-modal').find('#continue_delete_guest').removeAttr('id');
                $('.guest-modal').find('.update_delete_guest').css('visibility', 'hidden');
                $('.guest-modal').find('.update_delete_guest').text('Delete');
                $('.guest-modal').find('.hide-if-continue').css('display', 'block');
                $('.loadingDiv').hide();
                close_alert();
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        }); 
    }
});

$(document).on('click', '.back_delete', function(){
    $('.guest-modal').find('#continue_delete_guest').addClass('update_delete_guest');
    $('.guest-modal').find('#continue_delete_guest').removeAttr('id');
    $('.guest-modal').find('.update_delete_guest').css('visibility', 'hidden');
    $('.guest-modal').find('.update_delete_guest').text('Delete');
    $('.guest-modal').find('#back_delete_span').css('visibility', 'hidden');
    $('.guest-modal').find('.hide-if-continue').css('display', 'block');
    $('.guest-modal').find('.delete_guest_alert').html('');
    $('.guest-modal').find('#delete_guest_dropdown').val('');
});