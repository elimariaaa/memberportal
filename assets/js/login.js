$(document).on('click', '#brunchwork_apply', function(){
    //ajax to get cities
    $.ajax({
        url: window.base_url + 'member/get_cities',
        type: "GET",
        dataType:'JSON',
        success: function(response) {
            console.log(response);
            var cities = '';
            var cities_holder = [];
            cities_holder.push({
                'text': 'Select one.',
                'value': '',
                'disabled': 'disabled'
            });
            $.each( response.cities, function( key, value ) {
                cities_holder.push({
                    'text': value.city,
                    'value': 'https://brunchwork.com/membership-' + value.city.toLowerCase()
                });
            });
            console.log(cities_holder);
            
            bootbox.prompt({
                title: "Choose City",
                inputType: 'select',
                message: '<p class="text-center">Please wait while we do something...</p>',
                inputOptions: cities_holder,
                callback: function (result) {
                    console.log(result);
                    if(result == ''){
                        $('form.bootbox-form').prepend('<div class="alert alert-danger text-left city_alert" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please select a valid city.</div>');
                        return false;
                    } else if(result == null) {
                        $(this).modal('hide');
                    } else {
                        $('.city_alert').hide();
                        location.href= result;
                    }
                }
            });
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
          console.log("There was an error: " + errorThrown);
        }
      });
});