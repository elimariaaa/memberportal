<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class GuestpassModel extends CI_Model
{
    function get_active_users(){
        $this->load->library('braintree_lib');
        //get all plans
        $all_plans = $this->get_braintree_plans();
        
        $braintree_plans = $this->braintree_lib->all_plans();
        
        //get all active users
        $active_users = $this->db->query("SELECT `id`, `email`, `braintree_plan_id`, `first_bill_date`, `next_guest_pass_month` FROM `ci_users` WHERE MONTH((CONCAT(YEAR(CURRENT_DATE),'-',`next_guest_pass_month`))) = MONTH(CURRENT_DATE) AND DATE((CONCAT(YEAR(CURRENT_DATE),'-',`next_guest_pass_month`))) <= CURDATE() AND `active` = 1 AND `braintree_plan_id` != 'unlimited'")->result_array();
        $email_body = '';
        //for live brunchwork and brandfuss
        date_default_timezone_set('America/Los_Angeles');
        
        if($active_users){
            foreach($active_users AS $active_data):
                $next_guest_pass_month = $active_data['next_guest_pass_month'];
                $current_month = date('m-d');
                
                if($next_guest_pass_month < $current_month){
                    
                    //'date is in the past'; die();
                    //update next guest pass month if date has passed
                    
                    $updated_guest_pass_month = $this->db->query("UPDATE `ci_users` SET `next_guest_pass_month`= DATE_FORMAT(DATE_ADD(CONCAT(YEAR(CURRENT_DATE),'-',`next_guest_pass_month`), INTERVAL 6 MONTH),'%m-%d') WHERE `id` = ".$active_data['id']);
                    if($updated_guest_pass_month){
                        $email_body .= "Updated_guest_pass_month for ".$active_data['email']."<br />";
                    }
                } else {
                    //get plan ID
                    $braintree_plan_id = $active_data['braintree_plan_id'];
                    
                    if(!$active_data['braintree_plan_id']){
                        $braintree_customer = $this->braintree_lib->find_client('email', $active_data['email']);
        
                        if($braintree_customer){
                            foreach($braintree_customer AS $customer_details){
                                $customer_braintree_id = $customer_details->id;
                                $credit_card_transactions = $customer_details->creditCards;
                                foreach($credit_card_transactions AS $transactions){
                                    $subscriptions = $transactions->subscriptions;
                
                                    foreach($subscriptions AS $get_subs){
                                        $planId = $get_subs->planId;
                                    }
                                }
                            }
                            //update plan ID
                            $braintree_update_data = array('braintree_plan_id', $planId);
                            $update_plan_id = $this->db->where('id', $active_data['id'])->update('ci_users', $braintree_update_data);
                            if($update_plan_id){
                                $braintree_plan_id = $planId;
                            }
                        }
                    }
                    
                    $date_today = date('m-d');
                    if($next_guest_pass_month == $date_today){
                        //add guest pass
                        //get plan name
                        foreach($braintree_plans AS $get_plans){
                            if($braintree_plan_id == $get_plans->id){
                                $plan_name = $get_plans->name;
                            }
                        }
                        
                        //print_r($all_plans);
                        $key = array_search($plan_name, array_column($all_plans, 'plan'));
                        $guest_pass = $all_plans[$key]['guest_pass'];
                        //add the additional guest pass
                        $update_guest_pass = $this->db->query("UPDATE `ci_users` SET `remaining_guest_pass` = (`remaining_guest_pass` + ".$guest_pass."), `next_guest_pass_month`= DATE_FORMAT(DATE_ADD(CONCAT(YEAR(CURRENT_DATE),'-',`next_guest_pass_month`), INTERVAL 6 MONTH),'%m-%d') WHERE `id` = ".$active_data['id']);
                        if($update_guest_pass){
                            $email_body .= "Added ".$guest_pass." guest pass and updated_guest_pass_month for ".$active_data['email']."<br />";
                        }
                    }
                }
            endforeach;
        } else {
            //get all active users without next guest pass month
            $active_users = $this->db->query("SELECT `id`, `email`, `braintree_plan_id`, `first_bill_date`, `next_guest_pass_month` FROM `ci_users` WHERE `next_guest_pass_month` IS NULL AND `active` = 1 AND `first_bill_date` IS NOT NULL")->result_array(); //AND `braintree_plan_id` != 'unlimited'
            if($active_users){
                foreach($active_users AS $active_data):
                    $next_guest_pass_month = $this->insert_guest_pass_month($active_data['id'], $active_data['first_bill_date']);
                    $current_month = date('m-d');

                    if($next_guest_pass_month < $current_month){
                    
                        //'date is in the past'; die();
                        //update next guest pass month if date has passed
                        
                        $updated_guest_pass_month = $this->db->query("UPDATE `ci_users` SET `next_guest_pass_month`= DATE_FORMAT(DATE_ADD(CONCAT(YEAR(CURRENT_DATE),'-',`next_guest_pass_month`), INTERVAL 6 MONTH),'%m-%d') WHERE `id` = ".$active_data['id']);
                        if($updated_guest_pass_month){
                            $email_body .= "Updated_guest_pass_month for ".$active_data['email']."<br />";
                        }
                    } else {
                        //get plan ID
                        $braintree_plan_id = $active_data['braintree_plan_id'];
                        
                        if(!$active_data['braintree_plan_id']){
                            $braintree_customer = $this->braintree_lib->find_client('email', $active_data['email']);
            
                            if($braintree_customer){
                                foreach($braintree_customer AS $customer_details){
                                    $customer_braintree_id = $customer_details->id;
                                    $credit_card_transactions = $customer_details->creditCards;
                                    foreach($credit_card_transactions AS $transactions){
                                        $subscriptions = $transactions->subscriptions;
                    
                                        foreach($subscriptions AS $get_subs){
                                            $planId = $get_subs->planId;
                                        }
                                    }
                                }
                                //update plan ID
                                $braintree_update_data = array('braintree_plan_id', $planId);
                                $update_plan_id = $this->db->where('id', $active_data['id'])->update('ci_users', $braintree_update_data);
                                if($update_plan_id){
                                    $braintree_plan_id = $planId;
                                }
                            }
                        }
                        
                        $date_today = date('m-d');
                        if($next_guest_pass_month == $date_today){
                            //add guest pass
                            //get plan name
                            foreach($braintree_plans AS $get_plans){
                                if($braintree_plan_id == $get_plans->id){
                                    $plan_name = $get_plans->name;
                                }
                            }
                            
                            //print_r($all_plans);
                            $key = array_search($plan_name, array_column($all_plans, 'plan'));
                            $guest_pass = $all_plans[$key]['guest_pass'];
                            //add the additional guest pass
                            $update_guest_pass = $this->db->query("UPDATE `ci_users` SET `remaining_guest_pass` = (`remaining_guest_pass` + ".$guest_pass."), `next_guest_pass_month`= DATE_FORMAT(DATE_ADD(CONCAT(YEAR(CURRENT_DATE),'-',`next_guest_pass_month`), INTERVAL 6 MONTH),'%m-%d') WHERE `id` = ".$active_data['id']);
                            if($update_guest_pass){
                                $email_body .= "Added ".$guest_pass." guest pass and updated_guest_pass_month for ".$active_data['email']."<br />";
                            }
                        }
                    }
                endforeach;
            } else {
                $email_body .= 'No data to update guest pass.';
            }
        }
        date_default_timezone_set('UTC');
        return $email_body;
    }

    function get_braintree_plans(){
        return $this->db->get('ci_braintree_plan')->result_array();
    }

    function insert_guest_pass_month($user_id = false, $first_bill_date = false){
        $first_query = "SELECT DATE_ADD('".$first_bill_date."', INTERVAL 6 MONTH) AS next_guest_pass FROM `ci_users` WHERE `id` = ".$user_id;
        $next_guest_pass = $this->db->query($first_query)->row()->next_guest_pass;
        $date_month = date("m-d", strtotime($next_guest_pass));
        $query = "UPDATE `ci_users` SET `next_guest_pass_month` = '".$date_month."' WHERE id = ".$user_id;
        $update = $this->db->query($query);
        if($update){
            $this->db->where('id', $user_id);
            return $this->db->get('ci_users')->row()->next_guest_pass_month;
        }
    }
}