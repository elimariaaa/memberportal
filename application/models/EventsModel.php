<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class EventsModel extends CI_Model
{
    function get_attendees($rowno,$rowperpage){
        $this->load->library('braintree_lib');
        $get_all_plans = $this->braintree_lib->all_plans();
        $test_arr = [];
        foreach($get_all_plans AS $data):
            $test_arr[] = array('plan_id' => $data->id, 'plan_name' => $data->name);
        endforeach;
        $user = $this->ion_auth->user()->row();
        $this->db->select('u.profile_image, u.first_name, u.last_name, u.position, u.company, u.industry, u.braintree_plan_id');
        $this->db->where('event_id =', $this->input->get('event_id'));
        //$this->db->where('user_id !=', $user->id);
        $this->db->from('ci_rsvp r');
        $this->db->join('ci_users u', 'u.id = r.user_id', 'left');
        $this->db->limit($rowperpage, $rowno);
        $this->db->order_by('r.id', 'desc');
        $result = $this->db->get()->result_array();
        $attendee_list = array();
        $index = 0;
        foreach($result AS $event_data){
            $key = array_search($event_data['braintree_plan_id'], array_column($test_arr, 'plan_id'));
            $attendee_list[] = array(
                        'profile_image' => ($event_data['profile_image']) ? $event_data['profile_image']:null,
                        'first_name' => ($event_data['first_name']) ? $event_data['first_name']:null,
                        'last_name' => ($event_data['last_name']) ? $event_data['last_name']:null,
                        'position' => ($event_data['position']) ? $event_data['position']:null,
                        'company' => ($event_data['company']) ? $event_data['company']:null,
                        'industry' => ($event_data['industry']) ? $event_data['industry']:null,
                        'braintree_plan_id' => ($event_data['braintree_plan_id']) ? $test_arr[$key]['plan_name']:null
                    );
            $index++;
        }
        //echo "<pre>";print_r($attendee_list); die();
        return $attendee_list;
    }

    // Select total records
    public function getrecordCount() {
        $this->db->select('count(*) as allcount');
        $this->db->where('event_id =', $this->input->get('event_id'));
        $this->db->from('ci_rsvp');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }

    function add_guest(){
        $user = $this->ion_auth->user()->row();
        $first_name = html_escape($this->input->post('guest_first_name'));
        $last_name = html_escape($this->input->post('guest_last_name'));
        $email_address = html_escape($this->input->post('guest_email'));
        $event_id = html_escape($this->input->post('event_id'));
        $data = array('success' => false, 'message' => '', 'warning_message' => '', 'remaining_guest_pass' => '', 'guest_id' => 0);
        //check if there's existing guest for certain user
        $count_existing = $this->check_guest($email_address);
        $data['remaining_guest_pass'] = $this->check_guest_pass();
        if($count_existing > 0){
            $data['message'] = 'The guest data you entered already exists. Please enter a different guest.';
        } else {
            //double check remaining guest pass
            $remaining_guest_pass = $this->check_guest_pass();
            if($remaining_guest_pass > 0 || $remaining_guest_pass == 'unlimited'){
                $insert_data = array(
                    'user_id' => $user->id,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email_address,
                    'event_id' => $event_id
                );
                $insert = $this->db->insert('ci_guest_pass', $insert_data);
                $data['guest_id'] = $this->db->insert_id();
                if($insert){
                    //update the number of guests
                    if($remaining_guest_pass != 'unlimited'){
                        $update = $this->db->query("UPDATE ci_users SET remaining_guest_pass = remaining_guest_pass - 1 WHERE id = ".$user->id);
                        if($update){
                            $data['remaining_guest_pass'] = $this->check_guest_pass();
                        }
                    }
                    $data['success'] = true;
                    $data['message'] = 'You have successfully added your guest in this event.';
                    
                    if($data['remaining_guest_pass'] == 0 && $data['remaining_guest_pass'] != 'unlimited'){
                        //get city
                        $city = $this->get_city($user->city_id);
                        $data['warning_message'] = 'You have no guest passes remaining. You can send an email to <strong><a href="mailto:concierge@brunchwork.com">concierge@brunchwork.com</a></strong> for more guest passes or <strong><a href="https://brunchwork.com/membership-'.strtolower($city).'">Upgrade</a></strong>.';
                    }
                }
            } else {
                $data['warning_message'] = 'You have no guest passes remaining. You can send an email to <strong><a href="mailto:concierge@brunchwork.com">concierge@brunchwork.com</a></strong> for more guest passes or <strong><a href="https://brunchwork.com/membership-">Upgrade</a></strong>.';
            }
        }
        return $data;
    }

    function check_guest($guest_email = false){
        return $this->db->where('email', $guest_email)->count_all_results('ci_guest_pass');
    }

    function check_guest_pass(){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->id)->get('ci_users')->row()->remaining_guest_pass;
    }

    function get_guests($event_id = false, $guest_id = false){
        if($event_id){
            $this->db->where('event_id', $event_id);
        }
        if($guest_id){
            $this->db->where('id', $guest_id);
        }
        $this->db->where('user_id', $this->session->userdata('user_id'));
        return $this->db->get('ci_guest_pass')->result_array();
    }
    
    function check_guests($event_id = false){
        if($event_id){
            $this->db->where('event_id', $event_id);
        }
        $this->db->where('user_id', $this->session->userdata('user_id'));
        return $this->db->count_all_results('ci_guest_pass');
    }

    function update_guest(){
        /*
        Array
        (
            [guest_first_name] => Testa
            [guest_last_name] => Test
            [guest_email] => karlpavia@yahoo.com
            [event_id] => 57546714773
            [guest_id] => 6
        )
        */
        $this->db->where('id', $this->input->post('guest_id'));
        $this->db->where('event_id', $this->input->post('event_id'));
        $set_data = array();  
        if($this->input->post('guest_first_name')){
            $set_data['first_name'] = $this->input->post('guest_first_name');
        }
        if($this->input->post('guest_last_name')){
            $set_data['last_name'] = $this->input->post('guest_last_name');
        }
        if($this->input->post('guest_email')){
            $set_data['email'] = $this->input->post('guest_email');
        }
        return $this->db->update('ci_guest_pass', $set_data);
    }

    function delete_guest(){
        /*
        Array
        (
            [event_id] => 57546714773
            [guest_id] => 7
        )
        */
        $this->db->where('event_id', $this->input->post('event_id'));
        $this->db->where('id', $this->input->post('guest_id'));
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $delete = $this->db->delete('ci_guest_pass');
        if($delete){
            //update the remaining guest pass
            $counter = $this->check_guest_pass();
            $counter += 1;
            $this->db->set('remaining_guest_pass', $counter);
            $this->db->where('id',$this->session->userdata('user_id'));
            $update_guest_pass = $this->db->update('ci_users');
            if($update_guest_pass){
                return true;
            }
        } else {
            return false;
        }
    }

    function get_city($city_id = false){
        $this->db->where('id', $city_id);
        $this->db->where('active', 1);
        return $this->db->get('ci_city_membership')->row()->city;
    }
}