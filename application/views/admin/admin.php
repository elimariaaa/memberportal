
<?php
$this->load->view('templates/memberportal_header');
?>
<div class="container">

<span class="admin_h1">Upcoming Events </span>
<a href="<?php echo base_url('logout'); ?>"><span style="float:right">Logout</span></a>


<?php
    foreach ($data as  $key => $value) { ?>
    <?php 
        $event_name = isset($value[0]['event_name']) ? $value[0]['event_name'] : $value[0]['alt_event_name']; 
        $event_month = isset($value[0]['event_month']) ? $value[0]['event_month'] : $value[0]['alt_event_month'];
        $event_date = isset($value[0]['event_date']) ? $value[0]['event_date'] : $value[0]['alt_event_date'];
        $event_city = isset($value[0]['event_city']) ? $value[0]['event_city'] : $value[0]['alt_event_city'];
    ?>
    <button type="button" class="btn btn-custom event_button" data-toggle="collapse" data-target="#demo<?php echo $key; ?>"><strong> <span style="color: #000;"><?php echo $event_name.' </span></strong> || '. $event_month.' '. $event_date. ' || ' . $event_city; ?></button>
    <div id="demo<?php echo $key; ?>" class="collapse">
        <table class="table member-guests-table" >
            <thead>
                <tr>
                    <th colspan="4">Eventname: <span style="color: #ffa500;"><?php echo  $event_name.' || Date: '. $event_month.' '. $event_date. ' || City: ' . $event_city; ?></span></th>
                </tr>
                <tr>
                    <th>Member Name</th>
                    <th>Member Email</th>
                    <th>Guest Name</th>
                    <th>Guest Email</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($value as $mem_det) { ?>
                <?php 
                $mem_att_name = isset($mem_det['attendee_first_name']) ?  $mem_det['attendee_first_name'].' '.$mem_det['attendee_last_name'].'(non-member)':  $mem_det['member_first_name'].' '.$mem_det['member_last_name'].'(Member)'; 
                $mem_att_email = isset($mem_det['attendee_email']) ?  $mem_det['attendee_email']:  $mem_det['member_email'];
                $guest_name = isset($mem_det['guest_first_name']) ?  $mem_det['guest_first_name']:  '-';
                $guest_email = isset($mem_det['guest_email'])?  $mem_det['guest_email']:  '-';

                ?>
                <tr>
                    <td><?php echo $mem_att_name; ?></td>
                    <td><?php echo $mem_att_email; ?></td>
                    <td><?php echo $guest_name; ?></td>
                    <td><?php echo $guest_email; ?></td>
                </tr>
                
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php }  ?>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.member-guests-table').DataTable( {
        // Processing indicator
        //"processing": true,
        // DataTables server-side processing mode
        //"serverSide": true,

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );



    /*$('#memListTable').DataTable({
        
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('members/getLists/'); ?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{ 
            "targets": [0],
            "orderable": false
        }]
    });*/
});
</script>
<?php
$this->load->view('templates/memberportal_footer');
?>