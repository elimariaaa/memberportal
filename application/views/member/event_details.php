<div class="card-deck">
    <div class="card no-radius">
        <div class="card-body">
            <div class="calendar-container container">
                <h5 class="card-title event-name"></h5>
                <hr />
                <p class="text-left event-description"></p>
            </div>
        </div>
    </div>
</div>
<div class="fifty-spacer"></div>
<div class="card-deck">
    <div class="card no-radius bg-pink">
        <div class="card-body">
            <div class="calendar-container container">
                <h5 class="card-title attendees-list"></h5>
                <hr />
                <div class="attendeeslist_holder"></div>
                <!-- Paginate -->
                <div style='margin-top: 10px;' id='pagination'></div>
            </div>
            <div class="hundred-spacer"></div>
            <div class="center-parent loadingDiv"><div class="center-container"></div></div>
        </div>
    </div>
</div>