<div class="container">
    <div class="row">
        <div class="col-md-12 justify-content-center align-items-center">
            <div class="box shadowed-box mb-2">
                <form class="form-signin text-center" method="POST">
                    <!--<img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" alt="">-->
                    <h1>
                        <span style="color: #FFA500;">brunch</span><span style="color: #808080;">work</span>
                    </h1>
                    <h2>MEMBER PORTAL</h2>
                    <div class="mb-4"></div>
                    <!--<span style="color: #FFA500;">Learn From Industry Leaders</span>-->
                    <div class="mb-4"></div>
                    <?php
                        if(isset($message) && $message != ''){
                    ?>
                    <div class="alert alert-<?php echo (isset($alert_type) && $alert_type != '') ? $alert_type:'info'; ?> text-left" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo $message; ?>
                    </div>
                    <?php            
                        }
                    ?>
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input type="email" name="username" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
                    <div class="mb-4"></div>
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                    <div class="row mp-1 mb-1">
                        <div class="col text-left">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember_me" value="remember-me">  <span style="font-size: 12px;">Remember me</span>
                                </label>
                            </div>
                        </div>
                        <div class="col text-right">
                            <a href="<?php echo base_url(); ?>forgot_password" style="font-size: 12px;">Forgot your password?</a>
                        </div>
                    </div>
                    <?php
                            $csrf = array(
                                'name' => $this->security->get_csrf_token_name(),
                                'hash' => $this->security->get_csrf_hash()
                                );
                    ?>
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    <button class="btn btn-lg btn-brunchwork btn-block btn-signin" type="submit">SIGN IN</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col justify-content-center align-items-center text-center">
            <!--<button type="button" class="btn btn-link" id="brunchwork_signup">Create Account</button> or -->
            <button type="button" class="btn btn-link" id="brunchwork_apply">Apply to be a member</button>
        </div>
    </div>
</div>
<!-- Bootbox -->
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/login.js?v=').VER_NO; ?>"></script>