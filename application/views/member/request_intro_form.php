<?php $user = $this->ion_auth->user()->row(); ?>
<div id="contact-member-form-content">
    <div class="container">
        <div class="ten-spacer"></div>
        <div class="contact_member_alert"></div>
        <form id="contact-reqintro-form" method="post">
            <div class="form-group">
                <label for="from_email">From</label>
                <input type="email" class="form-control no-radius" id="from_email" name="from_email" placeholder="From Email" value="<?php echo $user->email; ?>" required readonly>
            </div>
            <div class="form-group" >
                <label for="to_email">To</label>
                <input type="text" id="dir_to_email" name="to_email" class="form-control no-radius" value="<?php echo $to; ?>" required  readonly />
            </div>
 <!--            <div class="form-group" >
                <label for="to_email">To</label>
                <input type="text" id="dir_to_email_2" name="to_email_2" class="form-control no-radius" value="<?php //echo $to_2; ?>" required  />
            </div> -->
            <div class="form-group">
                <label for="subject_email">Subject</label>
                <input type="text" class="form-control no-radius" id="subject_email" name="subject_email" placeholder="Email Subject" value="Member connection request" required readonly>
            </div>
            <div class="form-group">
                <label for="requested_person">Requested Person</label>
                <input type="text" class="form-control no-radius" placeholder="requested_person" value="<?php echo $requested_person; ?>" required readonly>
            </div>
            <div class="form-group">
                <label for="body_email">Message</label>
                <textarea class="form-control no-radius" rows="5" id="body_email" name="body_email" required></textarea>
            </div>
            <div class="form-group">
                <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                ?>
                <input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input type="hidden" id="requested_person" name="requested_person" value="<?php echo $requested_person; ?>" />
                
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <button type="button" class="btn btn-lg send_req_intro_email btn-link" style="padding: 5px;">Send</button> | 
                        <button type="button" class="btn btn-lg clear_req_intro_email btn-link" style="padding: 5px;">Clear</button>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="float-right"><button type="button" class="btn btn-lg cancel_req_intro_email btn-link" style="padding: 5px;">Cancel</button></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script>
    $(document).ready(function(){
        $('.loadingDiv').hide();
    });
</script>