<?php $user = $this->ion_auth->user()->row(); ?>
<div id="add-guest-form-content">
    <div class="container">
        <div class="text-center">
            <div class="twenty-spacer"></div>
            <img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" />
            <div class="twenty-spacer"></div>
            <h4>Delete a Guest</h4>
            <?php
                if($guests){            
                    if(count($guests) > 0){
                        $first_name = '';
                        $last_name = '';
                        $email = '';
                        $dropdown_option = "<select class='form-control' id='delete_guest_dropdown'><option value='' selected disabled>Select guest to delete.</option>";
                        foreach($guests AS $guest_data):
                            $dropdown_option .= "<option data-first_name='".$guest_data['first_name']."' data-last_name='".$guest_data['last_name']."' data-email='".$guest_data['email']."' value='".$guest_data['id']."'>".$guest_data['first_name']." ".$guest_data['last_name']."</option>";
                        endforeach;
                        $dropdown_option .= "</select>";
                        $guest_id = '';
                        $hide = 'display: none;';
                    } else if(count($guests) == 1){
                        $first_name = $guests[0]['first_name'];
                        $last_name = $guests[0]['last_name'];
                        $email = $guests[0]['email'];
                        $guest_id = $guests[0]['id'];
                        $dropdown_option = '';
                        $hide = 'display: block;';
                    } else {
                        $hide = 'display: block;';
                    }
                }
            ?>
        </div>
        <div class="delete_guest_alert"></div>
        <div class="ten-spacer"></div>
        <form id="delete-guest-form" method="post">
            <?php
                if($dropdown_option != ''){
            ?>
            <div class="form-group hide-if-continue">
                <label for="choose_guest">Select Guest</label>
                <?php echo $dropdown_option; ?>
            </div>
            <?php        
                }
            ?>
            <div class="form-group hidden" style="<?php echo $hide; ?>">
                <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                ?>
                <input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input type="hidden" id="event_id" name="event_id" value="<?=$event_id;?>" />
                <input type="hidden" id="guest_id" name="guest_id" value="<?=$guest_id;?>" />
            </div>
            <div class="container ">
                <div class="row">
                    <div class="col-md text-left">
                        <button type="button" class="btn btn-lg update_delete_guest btn-link p-0 hidden" style="<?php echo $hide; ?>">Delete</button>
                    </div>
                    <div class="col-md text-right">
                        <span id="back_delete_span" style="visibility: hidden;"><button type="button" class="btn btn-lg back_delete btn-link p-0">Back</button>
                        <!--
                         | </span>
                        <button type="button" class="btn btn-lg cancel_delete_guest btn-link p-0">Cancel</button>
                        -->
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script>
    $(document).ready(function(){
        $('.loadingDiv').hide();
    });
</script>