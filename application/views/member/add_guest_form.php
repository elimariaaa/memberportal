<?php $user = $this->ion_auth->user()->row(); ?>
<div id="add-guest-form-content">
    <div class="container">
        <div class="text-center">
            <div class="twenty-spacer"></div>
            <img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" />
            <div class="twenty-spacer"></div>
            <h4>Add a Guest</h4>
            <?php
                if($guest_passes > 0){
            ?>
            <div id="with_guest_pass">
            <p>Enter the name and email of each guest you’d like to attend.</p>
            <p>You may bring up to <span class="available_guest_count"><?php echo $guest_passes; ?></span> guests.</p>
            </div>
            <?php        
                }
            ?>
        </div>
        <div class="ten-spacer"></div>
        <div class="add_guest_alert">
        <?php 
            if($rsvp != ''){
        ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Event RSVP successful.    
            </div>
        <?php    
            }
            if($guest_passes == 0 && $guest_passes != 'unlimited'){
                $disable_form = 'disabled';
        ?>
            <div class="alert alert-warning" role="alert">
            <!--
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            -->
                You have no guest passes remaining. You can send an email to <strong><a href="mailto:concierge@brunchwork.com">concierge@brunchwork.com</a></strong> for more guest passes or <strong><a href="https://brunchwork.com/membership-<?php echo strtolower($city); ?>" target="_blank">Upgrade</a></strong>.  
            </div>
        <?php       
            } else {
                $disable_form = '';
            }
        ?>
        </div>
        <form id="add-guest-form" method="post">
            <div class="form-group">
                <label for="guest_first_name">First Name</label>
                <input type="text" class="form-control no-radius" id="guest_first_name" name="guest_first_name" placeholder="First Name (required)" required <?php echo $disable_form;?> />
            </div>
            <div class="form-group">
                <label for="guest_last_name">Last Name</label>
                <input type="text" id="guest_last_name" name="guest_last_name" class="form-control no-radius" placeholder="Last Name (required)" required <?php echo $disable_form;?> />
            </div>
            <div class="form-group">
                <label for="guest_email">Email</label>
                <input type="email" class="form-control no-radius" id="guest_email" name="guest_email" placeholder="Email (required)" required <?php echo $disable_form;?> />
            </div>
            <div class="form-group">
                <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                ?>
                <input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input type="hidden" id="event_id" name="event_id" value="<?=$event_id;?>" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md p-0 text-center">
                        <button type="button" class="btn btn-lg save_only_guest btn-link p-1 small-font" <?php echo $disable_form;?>>Save</button> | 
                        <button type="button" class="btn btn-lg save_add_guest btn-link p-1 small-font" <?php echo $disable_form;?> disabled>Add More Guests</button>
                        <!-- 
                        <button type="button" class="btn btn-lg cancel_add_guest btn-link p-1 small-font">Cancel</button> | 
                        <button type="button" class="btn btn-lg clear_add_guest btn-link p-1 small-font" <?php echo $disable_form;?>>Clear</button> | 
                        -->
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script>
    $(document).ready(function(){
        $('.loadingDiv').hide();
    });
</script>