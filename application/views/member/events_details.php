<script src="<?php echo base_url('assets/js/jquery.fancybox.pack.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.fancybox-media.js?v=').VER_NO; ?>"></script>
<link href="<?php echo base_url('assets/css/jquery.fancybox.css?v='.VER_NO); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/lightslider.css?v='.VER_NO); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/lightslider.js?v=').VER_NO; ?>"></script>
<style>
    div.container{
        max-width: 100%;
        padding: 0;
        margin-right: auto;
        margin-left: auto;
    }
    ul{
        list-style: none outside none;
        padding-left: 0;
        margin: 0;
    }
    .demo .item{
        margin-bottom: 60px;
    }
    .content-slider li{
        background-color: #ed3020;
        text-align: center;
        color: #FFF;
    }
    .content-slider h3 {
        margin: 0;
        padding: 70px 0;
    }
    .demo{
        width: 800px;
    }
</style>
<script>
    $(document).ready(function(){
        $('.loadingDiv').hide();
        var slider = $('#content-slider').lightSlider({
            loop:true,
            keyPress:true,
            speed: 4000, //ms'
            loop: true,
            slideEndAnimation: true,
            pause: 5000,
        });
        slider.play(); 
        $('.fancybox-media').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            helpers : {
                media : {}
            }
        });
    });

    $('a.btn-scroll').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
        scrollTop: $(sectionTo).offset().top
        }, 1500);
    });
</script>
<?php
    $request_url = "https://www.eventbriteapi.com/v3/events/".$this->input->get('id')."?organizer.id=".EVENT_ORGANIZER."&token=".EVENT_TOKEN;
    $params = array('sort_by' => 'date', 'organizer.id' => EVENT_ORGANIZER, 'token' => EVENT_TOKEN);
    $context = stream_context_create(
                    array(
                        'http' => array(
                            'method'  => 'GET',
                            'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                            //'content' => http_build_query($params)
                        )
                    )
                );
    $json_data = file_get_contents( $request_url, false, $context );
    //$response = get_object_vars(json_decode($json_data));
    $response = json_decode($json_data, true);
    //echo "<pre>";
    //print_r($response);
    $event_date = date('Y-M-d', strtotime($response['start']['local']));
    $start_event_time = date('g:i A', strtotime($response['start']['local']));
    $end_event_time = date('g:i A', strtotime($response['end']['local']));
    $day = date('l', strtotime($response['start']['local']));
    $calendar = explode('-', $event_date);
    $whole_date = date('F j, Y', strtotime($response['start']['local']));
    
    $html = str_get_html($response['description']['html']);
?>
<div class="event-banner">
    <div class="event-intro">
        <h2 style="font-weight: bold;"><?php echo $response['name']['text']; ?></h2>
        <h2 style="font-weight: bold;" class="region"></h2>
        <h5 style="font-weight: bold;" class="calendar-month"><?php echo $day.', '.$whole_date; ?></h5>
        <h5 style="font-weight: bold;" class="calendar-month"><?php echo $start_event_time.' to '.$end_event_time; ?> (EST)</h5>
        <button type="button" class="btn rsvp_button event_details_rsvp" data-event_id = "<?php echo $response['id']; ?>">RSVP <i class="fa fa-angle-double-right"></i></button>
    </div>
    <div class="text-center"><a class="btn-scroll" href="#event-content">Scroll</a></div>
</div>
<div class="row" style="background:#f7f7f7">
    <div class="col-md-12">
        <div class="twenty-spacer"></div>
        <h2 class="text-center" style="font-size:40px; font-weight: bold;">SPECIAL GUESTS</h2>
    </div>
<?php
    // Find all images
    $img_counter = count($html->find('img'));
    if($img_counter == 2){
        $div = "<div class='col-md-6 text-center'>";
    } else {
        $div = "<div class='col-md-12 text-center'>";
    }
    foreach($html->find('img') as $element) 
    echo $div.'<h4>'.$element->title.'</h4><img width="280px" height="280px" class="img-thumbnail img-responsive" src="'.$element->src.'"/></div>';
?>
    <div class="col-md-12">
        <div class="twenty-spacer"></div>
    </div>
</div>
<div class="row">
    <section class="col-md-12 text-center event-details">
        <h1 style="font-size: 45px; font-weight: bold;">WELCOME TO BRUNCHWORK</h1>
        <h1 style="font-family: 'Crimson Text' !important; font-style: italic; line-height: 1.3em;font-size: 45px; font-weight: bold;">A selective community for up-and-coming executives, entrepreneurs and game-changers. Find the access, resources, and community you need to get ahead.</h1>
    </section>
    <section class="col-md-12 text-center event-details">
        <ul id="content-slider" class="content-slider">
            <li>
                <img src="assets/images/1.jpg" />
            </li>
            <li>
                <img src="assets/images/2.jpg" />
            </li>
            <li>
                <img src="assets/images/3.jpg" />
            </li>
            <li>
                <img src="assets/images/4.jpg" />
            </li>
            <li>
                <img src="assets/images/5.jpg" />
            </li>
            <li>
                <img src="assets/images/6.jpg" />
            </li>
            <li>
                <img src="assets/images/7.jpg" />
            </li>
        </ul>
    </section>
</div>
<div id="event-content" class="row" style="background:#f7f7f7">
    <div class="col-md-12 thirty-spacer"></div>
    <div class="col-md-3 text-right"><h1>As featured in</h1></div>
    <div class="col-md-1 text-center"><img src="assets/images/cnbc.png" /></div>
    <div class="col-md-2 text-center"><img src="assets/images/forbes.png" /></div>
    <div class="col-md-3 text-center"><img src="assets/images/fortune.png" /></div>
    <div class="col-md-3"><img src="assets/images/inc.png" /></div>
    <div class="col-md-12 thirty-spacer"></div>
</div>
<div class="event-banner">
    <div class="col-md-12 fifty-spacer"></div>
    <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8 event-desc">
        <?php 
            $content = preg_replace("/<img[^>]+\>/i", "", $html); 
            echo $content;
        ?>
    </div>
    <div class="col-md-2">
    </div>
    </div>
    <div class="col-md-12 fifty-spacer"></div>
</div>
<div style="background-color: #ffffff;">
    <div class="fifty-spacer"></div>
    <div class="row">
        <div class="col-md-12 text-center">
            <h1 style="font-size: 45px; font-weight: bold;">WE ARE YOUR SECRET CAREER WEAPON</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-sm text-center">
            <div class="p-4">
                <img class="img-responsive" src="assets/images/creative.png"/>
            </div>
            <h4 style="font-weight: bold">Tactical breakouts</h4>
            <p>Get real time feedback on your business ideas</p>
        </div>
        <div class="col-sm text-center">
            <div class="p-4">
                <img class="img-responsive" src="assets/images/collaboration.png"/>
            </div>
            <h4 style="font-weight: bold">Meaningful connections</h4>
            <p>Curated introductions that meet your goals</p>
        </div>
        <div class="col-sm text-center">
            <div class="p-4">
                <img class="img-responsive" src="assets/images/hand-shake.png"/>
            </div>
            <h4 style="font-weight: bold">Warm and welcoming</h4>
            <p>Most guests come solo and bond during the breakouts</p>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="fifty-spacer"></div>
    <div class="row">
        <div class="col-sm text-center">
            <a class="btn btn-how-it-works fancybox-media" href="https://www.youtube.com/watch?v=JUM5SatlHfA&autoplay=1&rel=0&controls=0&showinfo=0"><b><i class="far fa-play-circle"></i> How It Works</b></a>
        </div>
    </div>
    <div class="fifty-spacer"></div>
    <div class="fifty-spacer" style="background:#f7f7f7"></div>
    <div class="row" style="background:#f7f7f7">
        <div class="col-sm text-center nominate">
            <span style="font-size: 22px;">Get a 50% promo code when you <a href="https://brunchwork.com/vib-status-2">nominate 10 friends</a> to brunchwork</span>
            <div class="ten-spacer"></div>
            <div class="val"><span style="color:#ffa500">*</span> Valid for 1 ticket or 1 month of membership</div>
        </div>
    </div>
    <div class="fifty-spacer" style="background:#f7f7f7"></div>
</div>