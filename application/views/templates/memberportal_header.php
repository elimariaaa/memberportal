<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- favicon -->
		<link rel="icon" href="<?php echo base_url('assets/images/fav-icon.png'); ?>">
		<!-- jQuery CDN -->
		<script  src="https://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>
		<!-- jQuery local fallback -->
		<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"><\/script>')</script>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/select2-bootstrap4.css'); ?>" />
		<!-- Font Awesome CSS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<!-- Scrollbar CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
		
		<!-- Custom styles for signin template -->
		<?php if(!$this->ion_auth->logged_in()){?>
		<link href="<?php echo base_url('assets/css/members/signin.css?v='.VER_NO); ?>" rel="stylesheet">
		<?php } ?>
		<link href="<?php echo base_url('assets/css/members/styles.css?v='.VER_NO); ?>" rel="stylesheet">
		<title><?php echo $pagetitle;?></title>
		<script> window.base_url = '<?php echo base_url(); ?>';</script>
		 <link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
		<script src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
	</head>
	<body>
		<?php
			if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
				$user = $this->ion_auth->user()->row();
		?>
		<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
			<div id="sidebar-content">
				<div id="dismiss">
					<i class="fas fa-arrow-right"></i>
				</div>
				<div class="fifty-spacer"></div>
				<div class="text-center">
					<!-- linkedin profile pic -->
					<img src="<?php echo ($user->profile_image) ? base_url($user->profile_image) : base_url('assets/images/default.png'); ?>" class="img-thumbnail rounded-circle profile_pic" />
					<!-- full name -->
					<h2><?php echo $user->first_name.' '.$user->last_name; ?></h2>
					<!-- position - company -->
					<h5 class="text-secondary">
						<?php 
							echo ($user->position) ? $user->position : 'N/A';
							echo ' - ';
							echo ($user->company) ? $user->company : 'N/A'; 
						?>
					</h5>
					<div class="ten-spacer"></div>
					<!-- other details -->
					<ul class="list-group list-group-flush text-left profile-details">
						<!--
						<li class="list-group-item text-muted bg-pink"><i class="fas fa-map-pin"></i><span class="tab small">N/A</span></li>
						-->
						<li class="list-group-item text-muted bg-pink"><i class="far fa-calendar"></i><span class="tab small">Joined <?php echo (!$user->first_bill_date) ? 'N/A' : date('F j, Y', strtotime($user->first_bill_date)); ?></span></li>
						<li class="list-group-item text-muted bg-pink"><i class="fas fa-map-pin"></i><span class="tab small"><?php echo ($city) ? $city: 'N/A'; ?></span></li>
						<!--
						<li class="list-group-item text-muted bg-pink"><i class="far fa-envelope"></i></i><span class="tab small"><?php echo $user->email; ?></span></li>
						-->
					</ul>
					<div class="twenty-spacer"></div>
					<!-- edit profile button -->
					<div class="containter">
						<div class="row">
							<div class="col-md-12 mb-2"><button type="button" class="btn btn-brunchwork btn-lg edit-profile">Edit Profile</button></div>
							<!--<div class="col-md-12 mb-2"><button type="button" class="btn btn-brunchwork btn-lg contact-member">Contact Other Member</button></div>-->
						</div>
					</div>
				</div>
			</div>
			<div id="edit-sidebar-content" style="display:none;">
				<div class="container">
					<div class="update_profile_alert"></div>
					<div class="ten-spacer"></div>
					<form id="edit-profile" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="first_name">Profile Image</label>
							<div class="input-group">
								<span class="input-group-btn">
									<span class="btn btn-default btn-file btn-brunchwork">
										Browse <input type="file" name="image" id="image">
									</span>
								</span>
								<input type="text" class="form-control" readonly value="<?php echo ($user->profile_image) ? $user->profile_image:''; ?>">
							</div>
							<span style="font-size: 12px;font-weight: 500;">Please upload 150x150 image.</span>
							<?php
								if($user->profile_image != null){
							?>
							<div class="ten-spacer"></div>
							<img id='img-upload' class="img-thumbnail rounded-circle profile_pic" src="<?php echo base_url($user->profile_image); ?>"/>
							<?php
								}
							?>
						</div>
						<div class="form-group">
							<label for="first_name">First Name</label>
							<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $user->first_name; ?>" required>
						</div>
						<div class="form-group">
							<label for="last_name">Last Name</label>
							<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $user->last_name; ?>" required>
						</div>
						<!--
						<div class="form-group">
							<label for="acquisition_channel">Acquisition Channel</label>
							<input type="text" class="form-control" id="acquisition_channel" name="acquisition_channel" placeholder="Acquisition Channel" value="<?php echo $user->acquisition_channel; ?>">
						</div>
						-->
						<div class="form-group">
							<label for="industry">Industry</label>
							<select class="form-control" name="industry" id="industry">
								<option value="" selected disabled>Select one.</option>
								<?php
								foreach($industry AS $display_industry):
								echo "<option value=".$display_industry['industry'].">".$display_industry['industry']."</option>";
								endforeach;
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="company">Company</label>
							<input type="text" class="form-control" id="company" name="company" placeholder="Company" value="<?php echo $user->company; ?>">
						</div>
						<div class="form-group">
							<label for="position">Position</label>
							<input type="text" class="form-control" id="position" name="position" placeholder="Position" value="<?php echo $user->position; ?>">
						</div>
						<!--
						<div class="form-group">
							<label for="linkedin_pic">Linkedin Image URL</label>
							<input type="text" class="form-control" id="profile_linkedin_pic" name="linkedin_pic" placeholder="Linkedin Image URL" value="<?php echo $user->linkedin_pic; ?>">
						</div>
						-->
						<div class="form-group">
							<label for="phone">Phone</label>
							<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php echo $user->phone; ?>">
						</div>
						<div class="form-group">
							<?php
								$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
								);
							?>
							<input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<button type="submit" class="btn btn-lg save_profile btn-link">Save</button>
								</div>
								<div class="col-md-6 text-right">
									<div class="float-right"><button type="button" class="btn btn-lg cancel_profile btn-link">Cancel</button></div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div id="contact-member-content" style="display:none;">
				<div class="container">
					<div class="ten-spacer"></div>
					<div class="contact_member_alert"></div>
					<form id="contact-member-form" method="post">
						<div class="form-group">
							<label for="from_email">From</label>
							<input type="email" class="form-control" id="from_email" name="from_email" placeholder="From Email" value="<?php echo $user->email; ?>" required>
						</div>
						<div class="form-group">
							<label for="to_email">To</label>
							<select id="to_email" name="to_email" class="form-control" required></select>
						</div>
						<div class="form-group">
							<label for="subject_email">Subject</label>
							<input type="text" class="form-control" id="subject_email" name="subject_email" placeholder="Email Subject" required>
						</div>
						<div class="form-group">
							<label for="body_email">Message</label>
							<textarea class="form-control" rows="5" id="body_email" name="body_email" required></textarea>
						</div>
						<div class="form-group">
							<?php
								$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
								);
							?>
							<input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8">
									<button type="button" class="btn btn-lg send_email btn-link" style="padding: 5px;">Send</button> | 
									<button type="button" class="btn btn-lg clear_email btn-link" style="padding: 5px;">Clear</button>
								</div>
								<div class="col-md-4 text-right">
									<div class="float-right"><button type="button" class="btn btn-lg cancel_send_email btn-link" style="padding: 5px;">Cancel</button></div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</nav>
		</div>
		<div class="overlay"></div>
		<div class="hundred-spacer"></div>
		<div class="container">
			<nav class="navbar navbar-expand-md navbar-light fixed-top bg-light navbar-brunchwork">
				<a href="<?php echo base_url('dashboard'); ?>"><img class="navbar-brand" src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" alt="logo"></a>
				<?php
					if(strtolower($user->braintree_subscription_status) != 'canceled' || strtolower($user->braintree_subscription_status != 'past_due') || strtolower($user->braintree_subscription_status) != 'pending' || strtolower($user->braintree_subscription_status) != 'expired'){
				?>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
					<ul class="navbar-nav ml-auto brunchwork-right-nav">
						<?php
							if(strtolower($user->is_paused) != 1){
						?>
						<!-- DISABLED UNTIL FURTHER NOTICE
						<li class="nav-item active">
							<a class="nav-link" href="<?php echo base_url('dashboard'); ?>">Home <span class="sr-only">(current)</span></a>
						</li>
						-->
						<li class="nav-item member-dir-link <?php echo (current_url() == base_url('directory')) ? 'active':'';?>">
							<a class="nav-link" href="<?php echo base_url('directory'); ?>">Member Directory</a>
						</li>
						<li class="nav-item events-link <?php echo (current_url() == base_url('events'))? 'active':'';?>">
							<a class="nav-link" href="<?php echo base_url('events'); ?>">Events</a>
						</li>
						<li class="nav-item settings-link <?php echo (current_url() == base_url('settings'))? 'active':'';?>">
							<a class="nav-link" href="<?php echo base_url('settings'); ?>">Account Settings</a>
						</li>
						<?php
							}
						?>
						<!--
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('membership'); ?>">Membership</a>
						</li>
						-->
						<li class="nav-item dropdown img-dd">
							<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><img src="<?php echo ($user->profile_image) ? base_url($user->profile_image) : base_url('assets/images/default.png'); ?>" class="img-thumbnail rounded-circle linkedin_pic" /></a>
							<div class="dropdown-menu dropdown-menu-right brunchwork-dd" aria-labelledby="dropdown01">
								<?php
									if(strtolower($user->is_paused) != 1){
								?>
								<a class="dropdown-item contact-support-form" href="#">Support</a>
								<a class="dropdown-item" href="<?php echo base_url('terms'); ?>">Terms & Conditions</a>
								<a class="dropdown-item" href="<?php echo base_url('privacy_policy'); ?>">Privacy Policy</a>
								<?php
									}
								?>
								<a class="dropdown-item" href="<?php echo base_url('logout'); ?>">Logout</a>
							</div>
						</li>
					</ul>
				</div>
				<?php
					}
				?>
			</nav>
			<div id="content">
		<?php		
			}
		?>