		<?php
			if($this->ion_auth->logged_in()){
		?>
		</div> <!-- div class content ending -->
		</div> <!-- div class container ending -->
		<?php } ?>
		<a href="#0" class="cd-top js-cd-top b_to_top btn btn-lg btn-brunchwork"><i class="fas fa-arrow-up"></i></a>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<!-- Bootstrap JS local fallback -->
		<script>
			if(typeof($.fn.modal) === 'undefined') {
				document.write('<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"><\/script>');
				//document.write('<script src="<?php echo base_url('assets/js/select2.full.min.js'); ?>"><\/script>');
			}
		</script>
		<!-- Popper.JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
		<!-- jQuery Custom Scroller CDN -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		<!--Sidebar JS-->
		<script src="<?php echo base_url('assets/js/sidebar.js'); ?>"></script>
		<!-- Back to top JS-->
		<script src="<?php echo base_url('assets/js/back_to_top.js'); ?>"></script>
		<!-- JQuery Validator -->
		<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery-input-mask-phone-number.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/select2.full.min.js'); ?>"></script>
		<!-- Bootstrap CSS local fallback -->
		<script src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.flash.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.print.min.js"></script>
		<div id="bootstrapCssTest" class="hidden"></div>
		<script>
			$(function() {
				if ($('#bootstrapCssTest').is(':visible')) {
					$("head").prepend('<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">');
					$("head").prepend('<link rel="stylesheet" href="<?php echo base_url('assets/fontawesome/css/all.css'); ?>">');
				}

			});
		</script>
		<?php if($this->ion_auth->logged_in()){ ?>
		<footer class="footer blog-footer" style="display: none;">&copy; <?php echo date("Y"); ?> brunchwork. All rights reserved.</footer>
		<?php } ?>
	</body>
</html>