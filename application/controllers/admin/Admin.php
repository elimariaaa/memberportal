<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->model('EventsModel');
        $this->load->library("braintree_lib");
        $this->check_admin();

    }
    function check_admin(){
        if (!$this->ion_auth->is_admin())
        {
          $this->session->set_flashdata('message', 'You must be an admin to view this page');
          redirect('/dashboard');
        }

    }
    public function index(){
        $rsvp_data = $this->UserModel->get_all_rsvp();
        $event_ids = array();
        $all_event_ids = array();

        $all_event_ids = $this->get_all_events();
        //echo "<pre>"; print_r($rsvp_data); echo "</pre>"; exit;
        foreach ($all_event_ids as $event_id_index => $event_id) {
            # code...
            $event_details = $this->get_event_data($event_id);
            //$event_details = $this->get_event_data($value['event_id']);
            $event_date = date('Y-M-d', strtotime($event_details['start']['local']));
            $start_event_time = date('g:i A', strtotime($event_details['start']['local']));
            $end_event_time = date('g:i A', strtotime($event_details['end']['local']));
            $day = date('l', strtotime($event_details['start']['local']));
            $calendar = explode('-', $event_date);
            $whole_date = date('F j, Y', strtotime($event_details['start']['local']));
            $calendar_date = $calendar[2];
            $calendar_month = $calendar[1];
            //$eventbrite_event_details = $this->get_event_attendes($event_id);

            foreach ($rsvp_data as $value) {
                if($value['event_id'] == $event_id){
                    $guest_first_name  = $value['guest_first_name'];
                    $guest_last_name  = $value['guest_last_name'];
                    $guest_email  = $value['guest_email'];
                    $member_first_name  = $value['member_first_name'];
                    $member_last_name  = $value['member_last_name'];
                    $member_email  = $value['member_email'];
                    $event_ids[$event_id][]  = [
                        "guest_first_name" => $guest_first_name, //$value['guest_first_name'],
                        "guest_last_name" => $guest_last_name, //$value['guest_last_name'],
                        "guest_email" => $guest_email, //$value['guest_email'],
                        "member_first_name" => $member_first_name, //$value['member_first_name'],
                        "member_last_name" => $member_last_name, //$value['member_last_name'],
                        "member_email" => $member_email, //$value['member_email'],
                        "event_name" =>  $event_details['name']['text'],
                        "event_date" => $calendar_date,
                        "event_month" => $calendar_month,
                        "event_city" => $event_details['venue']['address']['city'],
                        // "attendee_first_name" => $attendee_details['profile']['first_name'],
                        // "attendee_last_name" => $attendee_details['profile']['last_name'],
                        // "attendee_email" => $attendee_details['profile']['email'],
                    ];
                }
                
            }

            

        }
        /*foreach ($rsvp_data as $event_id => $value) {

            $event_details = $this->get_event_data($value['event_id']);
            $event_date = date('Y-M-d', strtotime($event_details['start']['local']));
            $start_event_time = date('g:i A', strtotime($event_details['start']['local']));
            $end_event_time = date('g:i A', strtotime($event_details['end']['local']));
            $day = date('l', strtotime($event_details['start']['local']));
            $calendar = explode('-', $event_date);
            $whole_date = date('F j, Y', strtotime($event_details['start']['local']));
            $calendar_date = $calendar[2];
            $calendar_month = $calendar[1];

            $all_event_ids[] = $value['event_id'];
            $event_ids[$value['event_id']][]  = [
                "guest_first_name" => $value['guest_first_name'],
                "guest_last_name" => $value['guest_last_name'],
                "guest_email" => $value['guest_email'],
                "member_first_name" => $value['member_first_name'],
                "member_last_name" => $value['member_last_name'],
                "member_email" => $value['member_email'],
                "event_name" =>  $event_details['name']['text'],
                "event_date" => $calendar_date,
                "event_month" => $calendar_month,
                "event_city" => $event_details['venue']['address']['city']
            ];            
        }*/

        //print_r($all_event_ids); 
        foreach (array_unique($all_event_ids) as $eID) {
            $eventbrite_event_details = $this->get_event_attendes($eID);

            $event_details = $this->get_event_data($eID);
            $event_date = date('Y-M-d', strtotime($event_details['start']['local']));
            $start_event_time = date('g:i A', strtotime($event_details['start']['local']));
            $end_event_time = date('g:i A', strtotime($event_details['end']['local']));
            $day = date('l', strtotime($event_details['start']['local']));
            $calendar = explode('-', $event_date);
            $whole_date = date('F j, Y', strtotime($event_details['start']['local']));
            $calendar_date = $calendar[2];
            $calendar_month = $calendar[1];
            
            foreach ($eventbrite_event_details['attendees'] as $key => $attendee_details) {
                $event_ids[$eID][]  = [
                    "attendee_first_name" => $attendee_details['profile']['first_name'],
                    "attendee_last_name" => $attendee_details['profile']['last_name'],
                    "attendee_email" => $attendee_details['profile']['email'],
                    "alt_event_name" =>  $event_details['name']['text'],
                    "alt_event_date" => $calendar_date,
                    "alt_event_month" => $calendar_month,
                    "alt_event_city" => $event_details['venue']['address']['city'],
                ];
            }
        }

                
        //echo "<pre>"; print_r($event_ids); echo "</pre>"; exit; 
        //$this->data['pagetitle'] = 'brunchwork | Admin';

        $data = ["data" => $event_ids, 'pagetitle' => 'brunchwork | Admin'];
        $this->load->view('admin/admin', $data);
    }


    public function get_event_data($eventid){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
           
        //$token = "5JVTZDONWCOPI2WUIWST";
        //$organizer_id =  "7806405987";
        //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
        $request_url = "https://www.eventbriteapi.com/v3/events/".$eventid."?token=".EVENT_TOKEN."&expand=venue";
        //$params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);
        //$get_count = $response['pagination']['object_count'];
        //echo "<pre>"; printr($response); echo "</pre>";
        return $response;
    }

    public function get_event_attendes($event_id){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
           
        //$token = "5JVTZDONWCOPI2WUIWST";
        //$organizer_id =  "7806405987";
        //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
        $request_url = "https://www.eventbriteapi.com/v3/events/".$event_id."/attendees?token=".EVENT_TOKEN;
        //$params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);
        //$get_count = $response['pagination']['object_count'];

        /*echo "<pre>"; print_r($response['attendees'][0]['profile']); echo "</pre>";
         exit;*/
        return $response;
    }


    public function get_all_events(){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
           
        //$token = "5JVTZDONWCOPI2WUIWST";
        //$organizer_id =  "7806405987";
        //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
        $request_url = "https://www.eventbriteapi.com/v3/events/search/?sort_by=date&organizer.id=".EVENT_ORGANIZER."&token=".EVENT_TOKEN;
        //$params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);
        //$get_count = $response['pagination']['object_count'];
        
        foreach ($response['events'] as $event) {
            $event_ids[] = $event['id'];
        }
        //echo "<pre>"; print_r($event_ids); echo "</pre>"; //exit;
        return $event_ids;        
    }


    public function import(){
        $data = ['pagetitle' => 'brunchwork | Admin - Import Users'];
        $this->load->view('admin/import', $data);
    }

    public function excel(){
       $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $file_mimes = array('application/vnd.ms-excel', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if(isset($_FILES['upload_file']['name']) && in_array($_FILES['upload_file']['type'], $file_mimes)) {

            $arr_file = explode('.', $_FILES['upload_file']['name']);
            $extension = end($arr_file);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);

            $users_data= $spreadsheet->getActiveSheet()->toArray();
            unset($users_data[0]);

            foreach ($users_data as $row) {
                if($row[1]){
                    switch ($row[5]) {
                        case 'SF':
                            $city_id = 2;
                            break;
                        case 'NY':
                            $city_id = 1;
                            break;
                        case 'LA':
                            $city_id = 3;
                            break;
                        case 'BOS':
                            $city_id = 6;
                            break;
                        case 'Other':
                            $city_id = 7;
                            break;
                        default:
                            $city_id = 7;
                            break;
                    }
                    $data = array( 
                        'braintree_customer_id'  =>  $row[1], 
                        'braintree_subscription_id'  =>  $row[0], 
                        'first_name' =>  $row[2], 
                        'last_name'   =>  $row[3],
                        'username'   =>  $row[4],
                        'email'   =>  $row[4],
                        'city_id' => $city_id,
                        'industry' => $row[7],
                        'company' => $row[8],
                        'position' => $row[9],
                        'profile_image' => $row[14],
                        'active' => 1
                    );
                    try{
                        $this->db->insert('ci_users', $data);
                    }catch(Exception $e){
                        return;
                    }
                }
            }
            redirect('/directory');
        }
    }
}