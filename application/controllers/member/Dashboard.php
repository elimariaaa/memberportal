<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Dashboard extends Auth_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->model('EventsModel');
        $this->load->library("braintree_lib");
    }
    
    public function index()
    {
        $user = $this->ion_auth->user()->row();
        if($this->ion_auth->logged_in() && $user->is_paused != 1){
            if($user->is_paused != NULL){
                $data = array(
                    'is_paused' => NULL,
                );
                $this->ion_auth->update($user->id, $data);
            }
            redirect('directory', 'refresh');
            $this->data['pagetitle'] = 'brunchwork | Dashboard';
            //$this->render('member/dashboard'); //disabled until further notice
        } else {
            if($this->ion_auth->logged_in()  && $user->is_paused == 1){
                redirect('settings');
            } else {
                redirect('login');
            }
        }
    }

    private function printJSON($var){
        echo json_encode($var);
    }

    public function get_token()
    {
        return $token = $this->braintree_lib->create_client_token();
        //$this->printJSON($token);
    }

    function get_clients(){
        $this->printJSON($this->braintree_lib->get_all_clients());
    }

    function get_client_data(){
        $client_data = $this->braintree_lib->client_fetch_id('1499265334');
        print_r($client_data);
        foreach($client_data as $customer) {
            echo $customer->firstName.'';
        }
    }

    function active_subscriptions(){
        $active_subscriptions = $this->braintree_lib->active_subscriptions();
        $counter = 5;
        foreach($active_subscriptions as $subscription) {
            if($counter == 0){
                die();
            }
            echo 'Subscription ID: '.$subscription->id.'<br />';
            echo 'merchantAccountId: '.$subscription->merchantAccountId.'<br />';
            echo 'planId: '.$subscription->planId.'<br /><br />';

            $counter--;
        }
    }

    function update_brantree_plan_id(){
        if($this->ion_auth->logged_in()){
            if($this->ion_auth->in_group('admin')){
                $users = $this->UserModel->get_all_members_with_subscription();
                foreach($users AS $user_data){
                    $userdata = $this->braintree_lib->find_subscription($user_data['braintree_subscription_id']);
                    $subscription_status = $userdata->status; 
                    $plan_id = $userdata->planId;
                    $first_billing_date = $userdata->firstBillingDate;
                    $first_bill_date = $first_billing_date->format('Y-m-d');
                    //print_r($userdata); die();
                    $update_arr = array(
                                    'first_bill_date' => $first_bill_date,
                                    'braintree_subscription_status' => $subscription_status,
                                    'braintree_plan_id' => $plan_id
                                );
                    echo $update_plan = $this->UserModel->update_userdata($update_arr, $user_data['braintree_subscription_id']);
                }
            } else {
                redirect('directory');
            }
        } else {
            redirect('login', 'refresh');
        }
    }
    function settings(){
        //declare variables first
        $first_bill_date = '';
        $new_date = '';
        $first_bill_year = '';
        $first_bill_month = '';
        $first_bill_day = '';
        $current_membership_date = '';
        $new_renewal_date = '';
        $address = '';
        $credit_card_transactions = '';
        $customer_braintree_id = '';
        $planId = '';
        $next_billing_date = '';
        $plan_name = '';
        $country_name = '';
        $this->data['token'] = '';
        if($this->ion_auth->logged_in()){
            $this->data['pagetitle'] = 'brunchwork | Settings';
            $user = $this->ion_auth->user()->row();
            //get plan
            $plans = $this->braintree_lib->all_plans();
            //1. get customer id using email address of logged in user
            $braintree_customer = $this->braintree_lib->find_client('email', $user->email);
            
            //print_r($braintree_customer); die();
            if($braintree_customer){
                foreach($braintree_customer AS $customer_details){
                    $customer_braintree_id = $customer_details->id;
                    $credit_card_transactions = $customer_details->creditCards;
                    foreach($credit_card_transactions AS $transactions){
                        $subscriptions = $transactions->subscriptions;
    
                        foreach($subscriptions AS $get_subs){
                            $planId = $get_subs->planId;
                            $next_billing_date = $get_subs->nextBillingDate;
                        }
                    }
                    //address
                    $address = $customer_details->addresses;
                }
            }
            
            if($address){
                $this->data['address_button'] = 'Update';
                foreach($address AS $get_address){
                    $this->data['address_id'] = ($get_address) ? $get_address->id:'';
                    $this->data['street_address'] = ($get_address) ? $get_address->streetAddress:'';
                    $this->data['extended_address'] = ($get_address) ? $get_address->extendedAddress:'';
                    $this->data['locality'] = ($get_address) ? $get_address->locality:'';
                    $this->data['region'] = ($get_address) ? $get_address->region:'';
                    $this->data['postalCode'] = ($get_address) ? $get_address->postalCode:'';
                    $this->data['countryCodeAlpha2'] = ($get_address) ? $get_address->countryCodeAlpha2:'';
                    $this->data['countryCodeAlpha3'] = ($get_address) ? $get_address->countryCodeAlpha3:'';
                    $this->data['countryCodeNumeric'] = ($get_address) ? $get_address->countryCodeNumeric:'';
                    $this->data['countryName'] = $country_name = ($get_address) ? $get_address->countryName:'';
                }

            } else {
                $this->data['address_button'] = 'Add';
                $this->data['address_id'] = '';
                $this->data['street_address'] = '';
                $this->data['extended_address'] = '';
                $this->data['locality'] = '';
                $this->data['region'] = '';
                $this->data['postalCode'] = '';
                $this->data['countryCodeAlpha2'] = '';
                $this->data['countryCodeAlpha3'] = '';
                $this->data['countryCodeNumeric'] = '';
                $this->data['countryName'] = '';
            }

            if($credit_card_transactions){
                foreach($credit_card_transactions AS $get_transactions){
                    if($get_transactions->default == 1){
                        $this->data['cc_bin'] = $get_transactions->bin;
                        $this->data['token'] = $get_transactions->token;
                    }
                }
            } else {
                $this->data['cc_bin'] = '';
            }

            $transactions = $this->braintree_lib->get_transactions($customer_braintree_id);
            $this->data['transactions'] = ($transactions) ? $transactions : '';
            $transaction_ids_arr = array();
            foreach($transactions AS $transactions){
                $transaction_ids_arr[] = $transactions->id;
            }
            $this->data['transaction_count'] = count($transaction_ids_arr);

            //get plan name
            foreach($plans AS $get_plans){
                if($planId == $get_plans->id){
                    $plan_name = $get_plans->name;
                }
            }
            //renewal date
            $this->data['renewal_date'] = ($next_billing_date) ? $next_billing_date->format('F j, Y') : 'N/A'; 
            $this->data['subscriber_plan'] = $plan_name ? $plan_name : 'None';
            $this->data['email_address'] = $user->email;
            $this->data['states'] = $this->UserModel->get_states();
            $this->data['address'] = ($address) ? $address : '';

            //countries
            $this->data['countries'] = $this->UserModel->get_countries();
            $this->data['client_token'] = $this->get_token();
            $this->data['customer_braintree_id'] = $customer_braintree_id;
            // pause membership
            $pause_membership = 0;
            $unpause = '';
            if($this->input->get()){
                if($this->input->get('no_of_month')){
                    $pause_membership = $this->input->get('no_of_month'); 
                }
                if($this->input->get('unpause')){
                    $unpause = $this->input->get('unpause');
                }
            }
            $this->data['unpause'] = $unpause;
            $this->data['no_of_month'] = $pause_membership;
            /*
            $get_all_plans = $this->braintree_lib->all_plans();
            $test_arr = [];
            foreach($get_all_plans AS $data):
                $test_arr[] = array('plan_id' => $data->id, 'plan_name' => $data->name);
            endforeach;
            $key = array_search($user->braintree_plan_id, array_column($test_arr, 'plan_id'));
            print_r($test_arr[$key]);
            */
            $this->load->model('MemberModel');
            $guest_passes = $this->MemberModel->get_braintree_plans();
            $key = array_search($plan_name, array_column($guest_passes, 'plan'));
            $this->data['guest_pass'] = $this->EventsModel->check_guest_pass();//$guest_passes[$key]['guest_pass'];
            $this->render('member/account/settings');
        } else {
            redirect('login');
        }
    }
}