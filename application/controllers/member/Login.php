<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
		$this->create_admin();
	}

	function create_admin(){
		//echo "Hie";
	    $username = 'admin_bw@memberportal.com';
	    $password = 'password';
	    $email = 'admin_bw@memberportal.com';
	    $additional_data = array(
	                'first_name' => 'Admin',
	                'last_name' => 'Admin',
	                );
	    $group = array('1'); // Sets user to admin.

	    
	    $this->ion_auth->register($email, $password, $username, $additional_data, $group);
	   
	}

	public function login()
	{
        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
        {
			//echo "here"; die();
			//redirect('dashboard'); //disabled upon further notice
			redirect('directory');
        } else if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
        	redirect('admin');
        }else {
			
            $this->data['pagetitle'] = 'brunchwork | Login';
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $msg = $this->session->flashdata('message');
            $this->data['message'] = $msg['message'];
            $this->data['alert_type'] = $msg['alert_type'];
            if ($this->form_validation->run() === FALSE)
            {
                $this->render('member/login');
            } else {
				if($this->input->post('remember_me')){
					$remember = 1;
				} else {
					$remember = 0;
				}
                //$remember = (bool) $this->input->post('remember_me');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                if ($this->ion_auth->login($username, $password, $remember))
                {
					//check first if the member has canceled status
					$user = $this->ion_auth->user()->row();
					if(strtolower($user->braintree_subscription_status) == 'canceled'){
						$this->ion_auth->logout();
						$message = '<p>Your membership was cancelled before. Please restart your membership to access the portal.</p><p>Contact <a href="mailto:concierge@brunchwork.com">concierge@brunchwork.com</a> to restart your membership.</p>';
						$alert_type = 'warning';
						$status = 'canceled';
						session_destroy();
						session_start();
						$this->session->set_userdata(array('message' => $message, 'alert_type' => $alert_type, 'status' => $status));
						redirect('login', 'refresh');
					} else {
						//if($username == 'britney.medich@gmail.com'){//keyana.sarikhani@gmail.com
							$this->load->library("braintree_lib");
							$transactions = $this->braintree_lib->get_transactions($user->braintree_customer_id);
							$max_count = 1;
							$counter = 0;
							foreach($transactions AS $get_transactions):
								$payment_status = strtolower($get_transactions->status);
								break;
								/*
								if($counter < $max_count){
									$payment_status = strtolower($get_transactions->status);
								}
								*/
							endforeach;
							$declined_transaction_status = array(strtolower('SETTLEMENT_DECLINED'), strtolower('VOIDED'), strtolower('PROCESSOR_DECLINED'), strtolower('GATEWAY_REJECTED'), strtolower('FAILED'));
							if(in_array($payment_status, $declined_transaction_status)){
								//do not allow to login
								$this->ion_auth->logout();
								$message = '<p>Please update your card information here: <strong><a href="https://brunchwork.com/update-card-information">https://brunchwork.com/update-card-information</a></strong></p>.
								<p>If you believe this is an error, please email <strong><a href="mailto:concierge@brunchwork.com">concierge@brunchwork.com</a></strong>.</p>';
								$alert_type = 'warning';
								session_destroy();
								session_start();
								$this->session->set_userdata(array('message' => $message, 'alert_type' => $alert_type));
								redirect('login', 'refresh');
							}
						//}
					}
					$sid = session_id();
					if($sid) {
						//session_start();
					} else {
						session_start();
					}
					//session_start();
					//redirect('dashboard'); //disabled upon further notice
					if($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
        {
			//echo "here"; die();
			//redirect('dashboard'); //disabled upon further notice
			redirect('admin');
        } else {
        	redirect('directory');
        }
                }
                else
                {
                    $_SESSION['message'] = $this->ion_auth->errors();
                    $this->session->mark_as_flash(array('message' => $_SESSION['message'], 'alert_type' => 'error'));
                    redirect('login');
                }
            }
        }
	}

	public function logout()
	{
		$this->ion_auth->logout();
		session_destroy();
		redirect('login');
	}

	public function forgot_password(){
		$this->data['pagetitle'] = 'brunchwork | Forgot Password';
        
        $this->form_validation->set_rules('username', 'Username', 'required|valid_email');
		
        if ($this->form_validation->run() == false)
        {
            $this->data['type'] = $this->config->item('identity','ion_auth');
            // setup the input
            $this->data['identity'] = array('name' => 'username',
                'id' => 'identity',
            );
            
            if ( $this->config->item('identity', 'ion_auth') != 'email' ){
                $this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            }
            else
            {
                $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }
            
            // set any errors and display the form
            $msg = $this->session->flashdata('message');
        
            $this->data['message'] = $msg['message'];
            $this->data['alert_type'] = $msg['alert_type'];
            $this->render('member/forgot_password');
        }
        else
        {
            $identity_column = $this->config->item('identity','ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('username'))->users()->row();
            if(empty($identity)) {
                
                if($this->config->item('identity', 'ion_auth') != 'email')
                {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                }
                else
                {
                    $this->ion_auth->set_error('The email you entered is not in the database. Please try again.');
                }
                
                $array_message = array('message' => preg_replace("/[#]/", "", $this->ion_auth->errors()), 'alert_type' => 'danger');
			    $this->session->set_flashdata('message', $array_message);
                redirect("forgot_password", 'refresh');
            }
			
            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
            
            if ($forgotten)
            {
                // if there were no errors
                //$this->session->set_flashdata('message', preg_replace("/[#]/", "", $this->ion_auth->messages()));
                $array_message = array('message' => $this->ion_auth->messages(), 'alert_type' => 'success');
			    $this->session->set_flashdata('message', $array_message);
                redirect("login", 'refresh'); //we should display a confirmation page here instead of the login page
            }
            else
            {
                $array_message = array('message' => preg_replace("/[#]/", "", $this->ion_auth->errors()), 'alert_type' => 'danger');
			    $this->session->set_flashdata('message', $array_message);
                redirect("forgot_password", 'refresh');
            }
        }
    }
    
    public function reset_password($code = NULL)
	{
        $uri_segment = $this->uri->segment_array();
        $code = end($uri_segment);
        /*
		if (!$code)
		{
			show_404();
		}
        */
		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id' => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'New Password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id' => 'new_confirm',
                    'type' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'New Password Confirm',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['user_id'] = array(
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				//$this->_render_page('member' . DIRECTORY_SEPARATOR . 'reset_password', $this->data);
				$this->render('member/reset_password');
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
                        $array_message = array('message' => $this->ion_auth->messages(), 'alert_type' => 'success');
			            $this->session->set_flashdata('message', $array_message);
						redirect("login", 'refresh');
					}
					else
					{
                        $array_message = array('message' => $this->ion_auth->errors(), 'alert_type' => 'danger');
			            $this->session->set_flashdata('message', $array_message);
						redirect('reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
            // if the code is invalid then send them back to the forgot password page
            $array_message = array('message' => $this->ion_auth->errors(), 'alert_type' => 'danger');
			$this->session->set_flashdata('message', $array_message);
			redirect("forgot_password", 'refresh');
		}
    }
    
    public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
    }
    
    public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')){
			return TRUE;
		}
			return FALSE;
    }
    
    public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
