<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Guestpass extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('GuestpassModel');
        $this->load->library("braintree_lib");
    }
    
    public function index()
    {
        //get all active 
        $all_active = $this->GuestpassModel->get_active_users();
        $this->load->library('email');
        $this->email->from('noreply@brunchwork.com', 'brunchwork Member Portal');
        $this->email->to('marren26@gmail.com');
        $this->email->subject('brunchwork Member Portal - Update Guest Pass');
        $this->email->message($all_active);
        if ( ! $this->email->send())
        {
            echo 'Email not sent. '.$this->email->print_debugger();
        } else {
            echo 'Email sent. '.date('Y-m-d H:i:s');
        }
    }
}