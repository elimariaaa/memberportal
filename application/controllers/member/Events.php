<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Events extends Auth_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->model('MemberModel');
        $this->load->model('EventsModel');
        $this->load->library("braintree_lib");
        // Load Pagination library
        $this->load->library('pagination');
        if(!$this->ion_auth->logged_in()){
            redirect('login');
        }
    }
    
    public function index()
    {
        $user = $this->ion_auth->user()->row();
        if($this->ion_auth->logged_in()  && $user->is_paused != 1){
            if($user->is_paused != NULL){
                $data = array(
                    'is_paused' => NULL,
                );
                $this->ion_auth->update($user->id, $data);
            }
            $this->data['pagetitle'] = 'brunchwork | Events';
            if($this->session->flashdata('message')){
                $msg = $this->session->flashdata('message');
                $this->data['message'] = $msg['message'];
                $this->data['alert_type'] = $msg['alert_type'];
            } else {
                $this->data['message'] = '';
                $this->data['alert_type'] = '';
            }
            //get all RSVP
            $rsvpd_events = array();
            $rsvp_data = $this->UserModel->get_rsvp();
            foreach($rsvp_data AS $data):
                $rsvpd_events[] = $data['event_id'];
            endforeach;
            $this->data['rsvpd_events'] = $rsvpd_events;
            $this->data['cities'] = $this->MemberModel->get_all_city();
            $this->render('member/events');
        } else {
            if($this->ion_auth->logged_in()  && $user->is_paused == 1){
                redirect('membership');
            } else {
                redirect('login');
            }
        }
    }

    function authenticate_user(){
        if($this->ion_auth->logged_in()) {
            $code = $this->input->get('code');
            $post_url = "https://www.eventbrite.com/oauth/token";
            $params = array('code' => $code, 'client_secret' => EVENTBRITE_CLIENT_SECRET, 'client_id' => EVENTBRITE_APP_KEY, 'grant_type' => 'authorization_code');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
            curl_setopt($ch, CURLOPT_URL, $post_url);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
            
            //{"access_token":"W5QQ4XOTFYQXRCO4CG7C","token_type":"bearer"}
            //stdClass Object ( [access_token] => CG5ZO2GF7UJPM2J653I7 [token_type] => bearer )
            $json_data = json_decode($response);
            
            $token = $json_data->access_token;
            $update_data_arr = array('eventbrite_token' => $token);
            $push_token = $this->UserModel->update_user($update_data_arr);
            if($push_token){
                $_SESSION['message'] = 'You have successfully authorized our Eventbrite App.';
                $this->session->mark_as_flash(array('message' => $_SESSION['message'], 'alert_type' => 'success'));
                redirect('events');
            }
        } else {
            redirect('login');
        }
    }

    function rsvp(){
        if($this->ion_auth->logged_in()) {
            $rsvp_data = $this->UserModel->rsvp();
            if($rsvp_data){
                $this->email_rsvp('member',$rsvp_data);
                $data['success'] = true;
                $data['count'] = $this->UserModel->count_rsvp($this->input->post('event_id'));
            } else {
                $data['success'] = false;
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function cancel_rsvp(){
        if($this->ion_auth->logged_in()) {
            $rsvp_data = $this->UserModel->cancel_rsvp();
            if($rsvp_data){
                $data['success'] = true;
                //email member
                $event_id = $this->input->post('event_id');
                $this->email_cancel_rsvp('member', $event_id);
            } else {
                $data['success'] = false;
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function details(){
        if($this->ion_auth->logged_in()) {
            $this->load->view('member/events_details');
        } else {
            redirect('login');
        }
    }

    function loadRecord($rowno=0){
        if($this->ion_auth->logged_in()) {
            // Row per page
            $rowperpage = 10;
        
            // Row position
            if($rowno != 0){
            $rowno = ($rowno-1) * $rowperpage;
            }
        
            // All records count
            $allcount = $this->EventsModel->getrecordCount();
        
            // Get records
            $users_record = $this->EventsModel->get_attendees($rowno,$rowperpage);
        
            // Pagination Configuration
            $config['base_url'] = base_url().'member/events/loadRecord/';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;

            $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul></nav></div>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            // Initialize
            $this->pagination->initialize($config);
        
            // Initialize $data Array
            $data['pagination'] = $this->pagination->create_links();
            $data['result'] = $users_record;
            $data['row'] = $rowno;
        
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function get_plan(){
        if($this->ion_auth->logged_in()) {
            $get_all_plans = $this->braintree_lib->all_plans();
            $test_arr = [];
            foreach($get_all_plans AS $data):
                $test_arr[] = array('plan_id' => $data->id, 'plan_name' => $data->name);
            endforeach;
            echo json_encode($test_arr);
        } else {
            redirect('login');
        }
    }

    function guest_form(){
        if($this->ion_auth->logged_in()) {
            $user = $this->ion_auth->user()->row();
            //get city
            $data['city'] = $this->EventsModel->get_city($user->city_id);
            $data['event_id'] = $this->input->get('event_id');
            $data['guest_passes'] = ($user->remaining_guest_pass) ? $user->remaining_guest_pass : 0;
            $data['rsvp'] = ($this->input->get('rsvp')) ? $this->input->get('rsvp') : '';
            $this->load->view('member/add_guest_form', $data);
        } else {
            redirect('login');
        }
    }

    function add_guest(){
        if($this->ion_auth->logged_in()) {
            $data = array('success' => false, 'message' => '', 'warning_message' => '', 'remaining_guest_pass' => '');
            //double check if member has guest pass
            $user = $this->ion_auth->user()->row();
            $guest_pass = ($user->remaining_guest_pass) ? $user->remaining_guest_pass : 0;
            
            $data['remaining_guest_pass'] = $guest_pass;
            if($guest_pass > 0 || $guest_pass == 'unlimited'){
                /*
                Array
                (
                    [guest_first_name] => Test1
                    [guest_last_name] => Test2
                    [guest_email] => test@test.com
                )
                */
                $add_data = $this->EventsModel->add_guest();
                $data['success'] = $add_data['success'];
                $data['message'] = $add_data['message'];
                $data['warning_message'] = $add_data['warning_message'];
                $data['remaining_guest_pass'] = $add_data['remaining_guest_pass'];
                if($add_data['success'] == true){
                    //email guest
                    $this->email_rsvp('guest',false,$add_data['guest_id']);
                }
            } else {
                $user = $this->ion_auth->user()->row();
                //get city
                $city = $this->EventsModel->get_city($user->city_id);
                $data['warning_message'] = 'You have no guest passes remaining. You can send an email to <strong><a href="mailto:concierge@brunchwork.com">concierge@brunchwork.com</a></strong> for more guest passes or <strong><a href="https://brunchwork.com/membership-'.strtolower($city).'">Upgrade</a></strong>.';
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function get_attendees(){
        $data['count'] = $this->UserModel->count_rsvp($this->input->get('event_id'));
        echo json_encode($data);
    }

    function email_cancel_rsvp($type = false, $event_id = false, $guest_data = false){
        $next = ".";
        if($type == 'member'){
            $to_email = $this->session->userdata('email');
            $intro_subject = 'Cancel RSVP Notification for ';
            $intro = "You have successfully cancelled your RSVP to ";
        } else if($type == 'guest'){
            foreach($guest_data AS $guest):
                $to_email = $guest['email'];
                $member_id = $guest['user_id'];
            endforeach;
            //get member_data
            $user_data = $this->UserModel->get_user_data($member_id);
            $intro_subject = "Event Invitation Cancelled for ";
            $intro = $user_data->first_name." ".$user_data->last_name." cancelled your invitation to attend ";
        }

        //get details of event
        $request_url = "https://www.eventbriteapi.com/v3/events/".$event_id."?organizer.id=".EVENT_ORGANIZER."&token=".EVENT_TOKEN;
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        
        $response = json_decode($json_data, true);
        
        $subject = $intro_subject.$response['name']['text'];

        $this->email->from('noreply@brunchwork.com', 'brunchwork Member Portal');
        $this->email->to(BW_EMAIL);
        $this->email->bcc('marren26@gmail.com');
        $this->email->subject($subject);

        $html = "<p>Hello!</p>
        ".$intro." <strong><a href='https://brunchwork.com/next-event/?eb_event_id=".$event_id."' target='_blank'>".$response['name']['text']."</a></strong>".$next."
        
        
        - the brunchwork team";
        $this->email->message(nl2br($html));
        if(!$this->email->send()){
            return false;
        } else {
            return true;
        }
    }

    function email_rsvp($type = false, $rsvp_id = false, $guest_id = false){
        if($type == 'member'){
            $rsvp_data = $this->UserModel->get_email_rsvp($rsvp_id);
            $event_id = $rsvp_data->event_id;
            //get the email of the user
            $user_data = $this->UserModel->get_user_data($rsvp_data->user_id);
            $to_email = $user_data->email;
        } else if($type == 'guest'){
            //get guest data
            $guest_data = $this->EventsModel->get_guests(false, $guest_id);
            foreach($guest_data AS $guest){
                $event_id = $guest['event_id'];
                $to_email = $guest['email'];
                $member_id = $guest['user_id'];
            }
            //get member_data
            $user_data = $this->UserModel->get_user_data($member_id);
        }
        //get details of event
        $request_url = "https://www.eventbriteapi.com/v3/events/".$event_id."?organizer.id=".EVENT_ORGANIZER."&token=".EVENT_TOKEN;
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        
        $response = json_decode($json_data, true);
        if($type == 'member'){
            $subject = "RSVP Notification for ".$response['name']['text'];
            $intro = "You have successfully RSVP'd to";
            $next = ".";
        } else if($type == 'guest'){
            $subject = $user_data->first_name.' '.$user_data->last_name.' has invited you to attend '.$response['name']['text'];
            $intro = "You have been invited to attend";
            $next = " by <strong>".$user_data->first_name." ".$user_data->last_name."</strong>.";
        }

        $this->email->from('noreply@brunchwork.com', 'brunchwork Member Portal');
        $this->email->to(BW_EMAIL);
        $this->email->bcc('marren26@gmail.com');
        $this->email->subject($subject);

        $day = date('l', strtotime($response['start']['local']));
        $whole_date = date('F j, Y', strtotime($response['start']['local']));
        $start_event_time = date('g:i A', strtotime($response['start']['local']));
        $end_event_time = date('g:i A', strtotime($response['end']['local']));
        //get the venue
        $venue_url = "https://www.eventbriteapi.com/v3/venues/".$response['venue_id']."/?organizer.id=".EVENT_ORGANIZER."&token=".EVENT_TOKEN;
        $context = stream_context_create(
            array(
                'http' => array(
                    'method'  => 'GET',
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                    //'content' => http_build_query($params)
                )
            )
        );
        $venue_json_data = file_get_contents( $venue_url, false, $context );
        $venue_response = json_decode($venue_json_data, true);
        $html = "<p>Hello!</p>
        ".$intro." <strong><a href='https://brunchwork.com/next-event/?eb_event_id=".$event_id."' target='_blank'>".$response['name']['text']."</a></strong>".$next."
        
        Please find the details below:

        <strong>".$day.", ".$whole_date.", from ".$start_event_time." to ".$end_event_time." (EST)
        ".$venue_response['address']['localized_address_display']."</strong>
        
        
        - the brunchwork team";
        $this->email->message(nl2br($html));
        if(!$this->email->send()){
            return false;
        } else {
            return true;
        }
    }

    function edit_guest_form(){
        //get all the guests by the member
        if($this->ion_auth->logged_in()) {
            $data['guests'] = $this->EventsModel->get_guests($this->input->get('event_id'));
            $data['event_id'] = $this->input->get('event_id');
            $this->load->view('member/edit_guest_form', $data);
        } else {
            redirect('login');
        }
    }

    function delete_guest_form(){
        if($this->ion_auth->logged_in()) {
            $data['guests'] = $this->EventsModel->get_guests($this->input->get('event_id'));
            $data['event_id'] = $this->input->get('event_id');
            $this->load->view('member/delete_guest_form', $data);
        } else {
            redirect('login');
        }
    }

    function check_has_guest(){
        //get all the guests by the member
        if($this->ion_auth->logged_in()) {
            $data['guests'] = $this->EventsModel->get_guests($this->input->get('event_id'));
            $data['count_guests'] = $this->EventsModel->check_guests($this->input->get('event_id'));
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function update_guest(){
        if($this->ion_auth->logged_in()) {
            $update = $this->EventsModel->update_guest();
            //get all the guests by the member
            $data['guests'] = $this->EventsModel->get_guests($this->input->post('event_id'));
            $data['count'] = ($data['guests']) ? count($data['guests']):0;
            if($update){
                $data['success'] = true;
                $data['alert_type'] = 'success';
            } else {
                $data['success'] = false;
                $data['alert_type'] = 'warning';
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function delete_guest(){
        if($this->ion_auth->logged_in()) {
            //get guest data for email
            $guest_data = $this->EventsModel->get_guests($this->input->post('event_id'), $this->input->post('guest_id'));
            $data['delete'] = $this->EventsModel->delete_guest();
            if($data['delete']){
                $data['success'] = true;
                $data['alert_type'] = 'success';
                $data['guests'] = $this->EventsModel->get_guests($this->input->post('event_id'));
                $data['count'] = ($data['guests']) ? count($data['guests']):0;
                $data['alert_message'] = 'You have successfully deleted a guest.';
                //email guest
                $this->email_cancel_rsvp('guest', $this->input->post('event_id'), $guest_data);
            } else {
                $data['success'] = false;
                $data['alert_type'] = 'warning';
                $data['alert_message'] = 'Something went wrong. Please try again.';
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }
}